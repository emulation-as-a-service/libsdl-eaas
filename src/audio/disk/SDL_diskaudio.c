/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org

    This file written by Ryan C. Gordon (icculus@icculus.org)
*/
#include "SDL_config.h"

/* Output raw audio data to a file. */

#if HAVE_STDIO_H
#include <stdio.h>
#endif

#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>

#include "SDL_rwops.h"
#include "SDL_timer.h"
#include "SDL_audio.h"
#include "../SDL_audiomem.h"
#include "../SDL_audio_c.h"
#include "../SDL_audiodev_c.h"
#include "SDL_diskaudio.h"

/* The tag name used by DISK audio */
#define DISKAUD_DRIVER_NAME "disk"

/* environment variables and defaults. */
#define DISKENVR_TYPE     "SDL_DISKAUDIOTYPE"
#define DISKTYPE_FILE     "file"
#define DISKTYPE_PIPE     "pipe"
#define DISKENVR_OUTFILE  "SDL_DISKAUDIOFILE"

/* Logging Halpers */
#define DISKAUD_LOGF(_frmt_, ...) \
	printf("[DISK-AUDIO] "_frmt_, ##__VA_ARGS__)

/* Audio driver functions */
static int DISKAUD_OpenAudio(_THIS, SDL_AudioSpec *spec);
static void DISKAUD_WaitAudio(_THIS);
static void DISKAUD_PlayAudio(_THIS);
static Uint8 *DISKAUD_GetAudioBuf(_THIS);
static void DISKAUD_CloseAudio(_THIS);
static void DISKAUD_DeleteDevice(_THIS);


static inline const char* DISKAUD_GetOutputFilename(void)
{
	return SDL_getenv(DISKENVR_OUTFILE);
}


static void* DISKAUD_AllocateAndReset(size_t size)
{
	void* memory = SDL_malloc(size);
	if (memory != NULL)
		SDL_memset(memory, 0, size);

	return memory;
}


static int DISKAUD_OpenPipe()
{
	const char* pipename = DISKAUD_GetOutputFilename();

	/* NOTE: Opening the named pipe in non-blocking mode fails, until
	 *       this pipe is also opened for reading by the client. */

	/* Open the named pipe in non-blocking mode */
	const int fd = open(pipename, O_WRONLY | O_NONBLOCK);
	if (fd < 1) {
		/* Show the error, skipping the 'no process opened pipe for reading' error. */
		if (errno != ENXIO)
			DISKAUD_LOGF("Opening the pipe '%s' failed: %s\n", pipename, strerror(errno));

		return -1;
	}

	DISKAUD_LOGF("Streaming audio to pipe '%s'.\n", pipename);
	return fd;
}


static int DISKAUD_Available(void)
{
	const char *envr = SDL_getenv("SDL_AUDIODRIVER");
	if (envr && (SDL_strcmp(envr, DISKAUD_DRIVER_NAME) == 0))
		return 1;

	return 0;
}


static SDL_AudioDevice* DISKAUD_CreateDevice(int devindex)
{
	/* Initialize all variables that we clean on shutdown */
	SDL_AudioDevice* this = (SDL_AudioDevice*) DISKAUD_AllocateAndReset(sizeof(SDL_AudioDevice));
	if (this != NULL) {
		this->hidden = (SDL_PrivateAudioData*) DISKAUD_AllocateAndReset(sizeof(SDL_PrivateAudioData));
		if (this->hidden == NULL) {
			SDL_free(this);
			this = NULL;
		}
	}

	if (this == NULL) {
		SDL_OutOfMemory();
		return NULL;
	}

	const char* pipename = DISKAUD_GetOutputFilename();
	if (pipename == NULL) {
		DISKAUD_LOGF("No pipe name for audio-streaming was set.\n");
		return NULL;
	}

	/* Create the named-pipe */
	const int ret = mkfifo(pipename, S_IRUSR | S_IWUSR);
	if ((ret < 0) && (errno != EEXIST)) {
		DISKAUD_LOGF("Creation of named-pipe '%s' failed: %s\n", pipename, strerror(errno));
		return NULL;
	}

	/* Set the function pointers */
	this->OpenAudio = DISKAUD_OpenAudio;
	this->WaitAudio = DISKAUD_WaitAudio;
	this->PlayAudio = DISKAUD_PlayAudio;
	this->GetAudioBuf = DISKAUD_GetAudioBuf;
	this->CloseAudio = DISKAUD_CloseAudio;
	this->free = DISKAUD_DeleteDevice;

	return this;
}



AudioBootStrap DISKAUD_bootstrap = {
	DISKAUD_DRIVER_NAME, "direct-to-disk audio",
	DISKAUD_Available, DISKAUD_CreateDevice
};


static Uint8* DISKAUD_GetAudioBuf(_THIS)
{
	return(this->hidden->mixbuf);
}


static int DISKAUD_OpenAudio(_THIS, SDL_AudioSpec* spec)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	DISKAUD_LOGF("Initializing pipe-based audio-driver\n");

	/* Setup destination format */
	spec->format = AUDIO_S16LSB;
	spec->freq = 44100;
	spec->channels = 2;
	SDL_CalculateAudioSpec(spec);

	/* Allocate mixing buffer */
	hidden->mixcap = spec->size;
	hidden->mixbuf = (Uint8*) SDL_AllocAudioMem(hidden->mixcap);
	if (hidden->mixbuf == NULL)
		return -1;

	SDL_memset(hidden->mixbuf, spec->silence, hidden->mixcap);

	hidden->duration = ((Uint32) spec->samples * 1000) / (Uint32) spec->freq;
	hidden->timestamp = SDL_GetTicks();

	return 0;  /* We're ready to rock and roll. :-) */
}


static void DISKAUD_PlayAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;
	hidden->timestamp = SDL_GetTicks();

	/* Get descriptor for the named-pipe, when needed */
	if (hidden->audio_fd < 1) {
		hidden->audio_fd = DISKAUD_OpenPipe();
		return;
	}

	/* Write current audio-data to the opened pipe */
	size_t buflen = (this->convert.needed) ? this->convert.len_cvt : this->spec.size;
	Uint8* bufpos = DISKAUD_GetAudioBuf(this);
	while (buflen > 0) {
		const ssize_t written = write(hidden->audio_fd, bufpos, buflen);
		if (written < 1) {
			const char* pipename = DISKAUD_GetOutputFilename();
			if (errno == EPIPE) {
				DISKAUD_LOGF("Pipe '%s' was closed by the client! Stop audio-output.\n", pipename);
				close(hidden->audio_fd);
				hidden->audio_fd = 0;
			}
			else DISKAUD_LOGF("Writing to pipe '%s' failed: %s\n", pipename, strerror(errno));

			break;
		}

		buflen -= written;
		bufpos += written;
	}
}


static void DISKAUD_WaitAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	/* Wait until it is possible to write a full sound buffer */
	const Uint32 elapsed_time = SDL_GetTicks() - hidden->timestamp;
	if (elapsed_time < hidden->duration)
		SDL_Delay(hidden->duration - elapsed_time);
}


static void DISKAUD_CloseAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	if (hidden->mixbuf != NULL) {
		SDL_FreeAudioMem(hidden->mixbuf);
		hidden->mixbuf = NULL;
	}
	if (hidden->audio_fd > 0) {
		close(hidden->audio_fd);
		hidden->audio_fd = 0;
	}

	unlink(DISKAUD_GetOutputFilename());
}


static void DISKAUD_DeleteDevice(SDL_AudioDevice* device)
{
	SDL_free(device->hidden);
	SDL_free(device);
}
