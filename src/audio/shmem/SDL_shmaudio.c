/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "SDL_config.h"

#if HAVE_STDIO_H
	#include <stdio.h>
#endif

#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "SDL_timer.h"
#include "SDL_audio.h"
#include "../SDL_audiomem.h"
#include "../SDL_audio_c.h"
#include "../SDL_audiodev_c.h"
#include "SDL_shmaudio.h"

/* The tag name used by shared-memory audio */
#define SHMAUD_DRIVER_NAME "shmem"

/* Environment variables and defaults. */
#define SHMAUD_ENVR_SEMOUT_NAME  "SDL_SHMAUDIO_SEMOUT_NAME"
#define SHMAUD_ENVR_SEMIN_NAME   "SDL_SHMAUDIO_SEMIN_NAME"
#define SHMAUD_ENVR_SHMEM_NAME   "SDL_SHMAUDIO_SHMEM_NAME"

/* Audio driver functions */
static int SHMAUD_OpenAudio(_THIS, SDL_AudioSpec *spec);
static void SHMAUD_WaitAudio(_THIS);
static void SHMAUD_PlayAudio(_THIS);
static Uint8* SHMAUD_GetAudioBuf(_THIS);
static void SHMAUD_CloseAudio(_THIS);
static void SHMAUD_DeleteDevice(_THIS);


/* =============== HELPER FUNCTIONS =============== */

static inline const char* SHMAUD_GetSemaphoreOutName(void)
{
	return SDL_getenv(SHMAUD_ENVR_SEMOUT_NAME);
}


static inline const char* SHMAUD_GetSemaphoreInName(void)
{
	return SDL_getenv(SHMAUD_ENVR_SEMIN_NAME);
}


static inline const char* SHMAUD_GetMemoryName(void)
{
	return SDL_getenv(SHMAUD_ENVR_SHMEM_NAME);
}


static inline void SHMAUD_SetMemBufLength(Uint8* buffer, Sint32 length)
{
	*((Sint32*) buffer) = length;
}


static void* SHMAUD_AllocateAndReset(size_t size)
{
	void* memory = SDL_malloc(size);
	if (memory != NULL)
		SDL_memset(memory, 0, size);

	return memory;
}


/* =============== SHMAUDIO CALLBACKS =============== */

/* Audio driver bootstrap functions */
static int SHMAUD_Available(void)
{
	const char* envr = SDL_getenv("SDL_AUDIODRIVER");
	if (envr && (SDL_strcmp(envr, SHMAUD_DRIVER_NAME) == 0))
		return 1;

	return 0;
}


static SDL_AudioDevice* SHMAUD_CreateDevice(int devindex)
{
	/* Initialize all variables that we clean on shutdown */
	SDL_AudioDevice* this = (SDL_AudioDevice*) SHMAUD_AllocateAndReset(sizeof(SDL_AudioDevice));
	if (this != NULL) {
		this->hidden = (SDL_PrivateAudioData*) SHMAUD_AllocateAndReset(sizeof(SDL_PrivateAudioData));
		if (this->hidden == NULL) {
			SDL_free(this);
			this = NULL;
		}
	}

	if (this == NULL) {
		SDL_OutOfMemory();
		return NULL;
	}

	/* Set the function pointers */
	this->OpenAudio = SHMAUD_OpenAudio;
	this->WaitAudio = SHMAUD_WaitAudio;
	this->PlayAudio = SHMAUD_PlayAudio;
	this->GetAudioBuf = SHMAUD_GetAudioBuf;
	this->CloseAudio = SHMAUD_CloseAudio;
	this->free = SHMAUD_DeleteDevice;

	SDL_PrivateAudioData* hidden = this->hidden;
	const char* semout_name = SHMAUD_GetSemaphoreOutName();
	const char* semin_name = SHMAUD_GetSemaphoreInName();
	const char* shmem_name = SHMAUD_GetMemoryName();

	/* Remove the target names */
	sem_unlink(semout_name);
	sem_unlink(semin_name);
	shm_unlink(shmem_name);

	/* Open a new named semaphore for output */
	sem_t* semout = sem_open(semout_name, O_CREAT, S_IRWXU, 0);
	if (semout == SEM_FAILED) {
		printf("Opening the semaphore '%s' failed: %s\n", semout_name, strerror(errno));
		goto exit_with_error;
	}

	/* Open a new named semaphore for input */
	sem_t* semin = sem_open(semin_name, O_CREAT, S_IRWXU, 0);
	if (semin == SEM_FAILED) {
		printf("Opening the semaphore '%s' failed: %s\n", semin_name, strerror(errno));
		goto exit_with_error;
	}

	/* Open the named shared-memory */
	const int memfd = shm_open(shmem_name, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
	if (memfd <= 0) {
		printf("Opening the shared-memory '%s' failed: %s\n", shmem_name, strerror(errno));
		goto exit_with_error;
	}

	/* Set the variables */
	hidden->shmem_fd = memfd;
	hidden->semout = semout;
	hidden->semin = semin;

	return this;  /* Success */

	/* Cleanup on failure */
	exit_with_error: {

		SDL_free(hidden);
		SDL_free(this);

		if (semout != SEM_FAILED)
			sem_close(semout);

		if (semin != SEM_FAILED)
			sem_close(semin);

		if (memfd > 0)
			close(memfd);

		sem_unlink(semout_name);
		sem_unlink(semin_name);
		shm_unlink(shmem_name);

		return NULL;
	}
}



AudioBootStrap SHMAUD_bootstrap = {
	SHMAUD_DRIVER_NAME, "shared-memory audio",
	SHMAUD_Available, SHMAUD_CreateDevice
};


static Uint8* SHMAUD_GetAudioBuf(_THIS)
{
	return (this->hidden->mixbuf);
}


static int SHMAUD_OpenAudio(_THIS, SDL_AudioSpec* spec)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	/* Setup destination format */
	spec->format = AUDIO_S16LSB;
	spec->freq = 44100;
	spec->channels = 2;
	SDL_CalculateAudioSpec(spec);

	/* Setup misc variables */
	hidden->shmoff = sizeof(Sint32);
	hidden->bufcap = spec->size;
	hidden->duration = ((Uint32) spec->samples * 1000) / (Uint32) spec->freq;

	/* Resize and map the named shared-memory */
	{
		const size_t shmcap = hidden->bufcap + hidden->shmoff;

		/* Resize the shared-memory buffer */
		if (ftruncate(hidden->shmem_fd, shmcap) != 0) {
			printf("Resizing the shared-memory '%s' failed: %s\n", SHMAUD_GetMemoryName(), strerror(errno));
			return -1;
		}

		/* Map the named shared-memory */
		Uint8* buffer = mmap(NULL, shmcap, PROT_WRITE, MAP_SHARED, hidden->shmem_fd, 0);
		if (buffer == MAP_FAILED) {
			printf("Mapping the shared-memory '%s' failed: %s\n", SHMAUD_GetMemoryName(), strerror(errno));
			return -1;
		}

		hidden->shmbuf = buffer;
	}

	/* Allocate local mixing buffer */
	hidden->mixbuf = (Uint8*) SDL_AllocAudioMem(hidden->bufcap);
	if (hidden->mixbuf == NULL) {
		printf("Allocation of the local mixing-buffer failed!");
		return -1;
	}

	/* Initialize the audio buffers */
	SHMAUD_SetMemBufLength(hidden->shmbuf, 0);
	SDL_memset(hidden->shmbuf + hidden->shmoff, spec->silence, hidden->bufcap);
	SDL_memset(hidden->mixbuf, spec->silence, hidden->bufcap);

	hidden->timestamp = SDL_GetTicks();

	return 0;  /* We're ready to rock and roll. :-) */
}


static void SHMAUD_PlayAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;
	hidden->timestamp = SDL_GetTicks();

	const size_t buflen = (this->convert.needed) ? this->convert.len_cvt : this->spec.size;

	/* Copy data from local mixing-buffer to shared-memory */
	SHMAUD_SetMemBufLength(hidden->shmbuf, buflen);
	SDL_memcpy(hidden->shmbuf + hidden->shmoff, hidden->mixbuf, buflen);

	/* Signal to the consumer process, that data is available */
	sem_post(hidden->semout);
}


/* This function waits until it is possible to write a full sound buffer */
static void SHMAUD_WaitAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	/* Wait for the consumer process to consume data */
	sem_wait(hidden->semin);

	/* Wait, until sound-buffer becomes full */
	const Uint32 elapsed_time = SDL_GetTicks() - hidden->timestamp;
	if (elapsed_time < hidden->duration)
		SDL_Delay(hidden->duration - elapsed_time);
}


static void SHMAUD_CloseAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	if (hidden->audio_fd > 0) {
		close(hidden->audio_fd);
		hidden->audio_fd = 0;
	}

	if (hidden->shmem_fd > 0) {
		close(hidden->shmem_fd);
		hidden->shmem_fd = 0;
	}

	if (hidden->semout != NULL) {
		sem_close(hidden->semout);
		hidden->semout = NULL;
	}

	if (hidden->semin != NULL) {
		sem_close(hidden->semin);
		hidden->semin = NULL;
	}

	if (hidden->shmbuf != NULL) {
		munmap(hidden->shmbuf, hidden->bufcap + hidden->shmoff);
		hidden->shmbuf = NULL;
	}

	if (hidden->mixbuf != NULL) {
		SDL_FreeAudioMem(hidden->mixbuf);
		hidden->mixbuf = NULL;
	}

	sem_unlink(SHMAUD_GetSemaphoreOutName());
	sem_unlink(SHMAUD_GetSemaphoreInName());
	shm_unlink(SHMAUD_GetMemoryName());
}


static void SHMAUD_DeleteDevice(SDL_AudioDevice* device)
{
	SDL_free(device->hidden);
	SDL_free(device);
}
