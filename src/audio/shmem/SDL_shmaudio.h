/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _SDL_shmaudio_h
#define _SDL_shmaudio_h

#include "../SDL_sysaudio.h"
#include <semaphore.h>

/* Hidden "this" pointer for the video functions */
#define _THIS	SDL_AudioDevice *this

struct SDL_PrivateAudioData
{
	int audio_fd;             /* The file descriptor for the audio device */
	int shmem_fd;             /* The file descriptor for shared-memory */
	sem_t* semout;            /* Semaphore for audio-output */
	sem_t* semin;             /* Semaphore for control-input */
	Uint8* shmbuf;            /* Mapped shared-memory buffer */
	Uint8* mixbuf;            /* The local buffer for audio */
	Uint32 shmoff;            /* Offset of the audio-data inside of shmbuf */
	Uint32 bufcap;            /* The buffer's capacity */
	Uint32 duration;          /* The duration of a frame */
    Uint32 timestamp;         /* The timestamp of the last frame */
};

typedef struct SDL_PrivateAudioData SDL_PrivateAudioData;

#endif /* _SDL_shmaudio_h */
