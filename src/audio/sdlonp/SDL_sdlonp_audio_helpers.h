
#ifndef __SDL_sdlonp_audio_helpers_h
#define __SDL_sdlonp_audio_helpers_h

#include "../../video/sdlonp/SDL_sdlonp_ostream.h"

int SDLONP_SendAudioFormat(SDLONP_OutputStream* ostream, int locked);

#endif /* __SDL_sdlonp_audio_helpers_h */
