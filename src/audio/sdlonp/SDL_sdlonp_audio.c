/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#include "SDL_config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>

#include "SDL_thread.h"
#include "SDL_timer.h"
#include "SDL_audio.h"
#include "../SDL_sysaudio.h"
#include "../SDL_audiomem.h"
#include "../SDL_audio_c.h"
#include "SDL_sdlonp_audio.h"
#include "../../video/sdlonp/SDL_sdlonp_shared_data.h"

#include <guacamole/sdlonp/protocol.h>
#include <guacamole/sdlonp/protocol_types.h>

/* Hidden "this" pointer for the video functions */
#define _THIS  SDL_AudioDevice* this


/* The tag name used by SDLONP-Audio */
#define SDLONP_AUDIO_DRIVER_NAME  "sdlonp"

/* Environment variables */
#define SDLONP_ENV_SAMPLERATE  "SDL_SAMPLERATE"

/* Default audio sample-rate */
#define SDLONP_DEFAULT_SAMPLERATE  22050

/* Logging Halpers */
#define SDLONP_LOGF(_frmt_, ...) \
	{	struct timespec time;  \
		clock_gettime(CLOCK_MONOTONIC, &time);  \
		printf("%ld.%ld [SDLONP-AUDIO] "_frmt_"\n", time.tv_sec, time.tv_nsec, ##__VA_ARGS__); \
		syslog(LOG_USER | LOG_INFO, "[SDLONP-AUDIO] "_frmt_, ##__VA_ARGS__); }

#define SDLONP_ENABLE_DEBUG_MESSAGES 1

#ifdef SDLONP_ENABLE_DEBUG_MESSAGES
#	define SDLONP_DEBUGF(_frmt_, ...)  SDLONP_LOGF(_frmt_, __VA_ARGS__)
#else
#	define SDLONP_DEBUGF(_frmt_, ...)  { }
#endif /* SDLONP_ENABLE_DEBUG_MESSAGES */


/* Audio driver functions */
static int SDLONP_OpenAudio(_THIS, SDL_AudioSpec *spec);
static void SDLONP_WaitAudio(_THIS);
static void SDLONP_PlayAudio(_THIS);
static Uint8 *SDLONP_GetAudioBuf(_THIS);
static void SDLONP_CloseAudio(_THIS);
static void SDLONP_AUDIO_DeleteDevice(_THIS);


static int SDLONP_GetIntEnv(const char* name, int default_value)
{
	const char* value = SDL_getenv(name);
	if (value == NULL)
		return default_value;

	return SDL_atoi(value);
}


int SDLONP_SendAudioFormat(SDLONP_OutputStream* ostream, int locked)
{
	/* Send the new format to client */
	sdlonp_audio_format format = {
			.audio_rate = SDLONP_GetIntEnv(SDLONP_ENV_SAMPLERATE, SDLONP_DEFAULT_SAMPLERATE),
			.channels = 2,
			.bits_per_sample = 16,
	};

	if (locked == 0)
		SDLONP_LockOutputStream(ostream);

	const int retcode = sdlonp_write_audio_format(ostream->fd, &format);

	if (locked == 0)
		SDLONP_UnlockOutputStream(ostream);

	if (retcode != 0)
		SDLONP_LOGF("Sending new audio-format failed!");

	return retcode;
}


static int SDLONP_AUDIO_Available(void)
{
	const char *envr = SDL_getenv("SDL_AUDIODRIVER");
	if (envr && (SDL_strcmp(envr, SDLONP_AUDIO_DRIVER_NAME) == 0))
		return 1;

	return 0;
}


static SDL_AudioDevice* SDLONP_AUDIO_CreateDevice(int devindex)
{
	SDLONP_LOGF("Initializing SDLONP audio-driver...");

	if (SDLONP_InitSharedData() < 0) {
		SDLONP_LOGF("Initializing SDLONP audio-driver failed!");
		return NULL;
	}

	/* Create the audio-device */
	SDL_AudioDevice* device = (SDL_AudioDevice*) SDL_malloc(sizeof(SDL_AudioDevice));
	if (device == NULL) {
		SDL_OutOfMemory();
		goto cleanup_on_error;
	}

	SDL_memset(device, 0, sizeof(SDL_AudioDevice));

	/* Create the internal audio-data */
	SDL_PrivateAudioData* hidden = (SDL_PrivateAudioData*) SDL_malloc(sizeof(SDL_PrivateAudioData));
	if (hidden == NULL) {
		SDL_OutOfMemory();
		goto cleanup_on_error;
	}

	SDL_memset(hidden, 0, sizeof(SDL_PrivateAudioData));
	device->hidden = hidden;

	/* Set the function pointers */
	device->OpenAudio = SDLONP_OpenAudio;
	device->WaitAudio = SDLONP_WaitAudio;
	device->PlayAudio = SDLONP_PlayAudio;
	device->GetAudioBuf = SDLONP_GetAudioBuf;
	device->CloseAudio = SDLONP_CloseAudio;
	device->free = SDLONP_AUDIO_DeleteDevice;

	SDLONP_LOGF("SDLONP audio-driver was initialized successfully.");

	return device;

	/* Cleanup */
	cleanup_on_error: {

		SDLONP_CleanupSharedData();

		if (hidden != NULL)
			SDL_free(hidden);

		if (device != NULL)
			SDL_free(device);

		return NULL;
	}
}


static Uint8* SDLONP_GetAudioBuf(_THIS)
{
	return this->hidden->mixbuf;
}


static int SDLONP_OpenAudio(_THIS, SDL_AudioSpec* spec)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	/* Setup destination format */
	spec->format = AUDIO_S16LSB;
	spec->freq = SDLONP_GetIntEnv(SDLONP_ENV_SAMPLERATE, SDLONP_DEFAULT_SAMPLERATE);
	spec->channels = 2;
	SDL_CalculateAudioSpec(spec);

	/* Allocate mixing buffer */
	hidden->mixcap = spec->size;
	hidden->mixbuf = (Uint8*) SDL_AllocAudioMem(hidden->mixcap);
	if (hidden->mixbuf == NULL)
		return -1;

	SDL_memset(hidden->mixbuf, spec->silence, hidden->mixcap);

	hidden->duration = ((Uint32) spec->samples * 1000) / (Uint32) spec->freq;
	hidden->timestamp = SDL_GetTicks();

	return 0;  /* We're ready to rock and roll. :-) */
}


static void SDLONP_PlayAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;
	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	SDLONP_OutputStream* ostream = shared_data->output_stream;

	hidden->timestamp = SDL_GetTicks();

	SDLONP_LockOutputStream(ostream);

	/* Client already disconnected? */
	if (SDLONP_IsOutputStreamEnabled(ostream) == 0) {
		SDLONP_UnlockOutputStream(ostream);
		return;  /* Yes */
	}

	/* Write current audio-data to the output-pipe */
	sdlonp_audio_header header;
	header.buffer_length = (this->convert.needed) ? this->convert.len_cvt : this->spec.size;
	if (sdlonp_write_audio_data(ostream->fd, &header, SDLONP_GetAudioBuf(this)) != 0)
		SDLONP_LOGF("Writing audio-buffer to pipe failed!");

	SDLONP_UnlockOutputStream(ostream);
}


static void SDLONP_WaitAudio(_THIS)
{
	SDL_PrivateAudioData* hidden = this->hidden;

	/* Wait until it is possible to write a full sound buffer */
	const Uint32 elapsed_time = SDL_GetTicks() - hidden->timestamp;
	if (elapsed_time < hidden->duration)
		SDL_Delay(hidden->duration - elapsed_time);
}


static void SDLONP_CloseAudio(_THIS)
{
	SDLONP_LOGF("Closing SDLONP audio-driver...");

	SDL_PrivateAudioData* hidden = this->hidden;

	if (hidden->mixbuf != NULL) {
		SDL_FreeAudioMem(hidden->mixbuf);
		hidden->mixbuf = NULL;
	}

	SDLONP_LOGF("SDLONP audio-driver closed.");
}


static void SDLONP_AUDIO_DeleteDevice(SDL_AudioDevice* device)
{
	SDLONP_LOGF("Deleting SDLONP audio-driver...");

	SDLONP_CleanupSharedData();
	SDL_free(device->hidden);
	SDL_free(device);

	SDLONP_LOGF("SDLONP audio-driver deleted.");
}


AudioBootStrap SDLONP_AUDIO_bootstrap = {
	SDLONP_AUDIO_DRIVER_NAME, "sdlonp-audio",
	SDLONP_AUDIO_Available, SDLONP_AUDIO_CreateDevice
};
