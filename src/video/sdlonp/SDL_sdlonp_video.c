/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#include "SDL_config.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>

#include "SDL_video.h"
#include "../SDL_pixels_c.h"

#include "SDL_sdlonp_video.h"
#include "SDL_sdlonp_events.h"
#include "SDL_sdlonp_logging.h"
#include "SDL_sdlonp_shared_data.h"
#include "../../audio/sdlonp/SDL_sdlonp_audio_helpers.h"

#include <guacamole/sdlonp/protocol.h>
#include <guacamole/sdlonp/protocol_types.h>


/*
 *  SDL-Over-NamedPipe driver:
 *
 *  This allows you to use an SDL application directly over Linux's IPC mechanism.
 */


#define SDLONP_VIDEO_DRIVER_NAME  "sdlonp"

/* NTSC-Video Filters */
#define SDLONP_FILTER_SNES_NTSC  "snes-ntsc"

/* NTSC-Filter Presets */
#define SDLONP_FILTER_PRESET_MONOCHROME  "monochrome"
#define SDLONP_FILTER_PRESET_COMPOSITE   "composite"
#define SDLONP_FILTER_PRESET_SVIDEO      "svideo"
#define SDLONP_FILTER_PRESET_RGB         "rgb"

/* Configuration */
#define SDLONP_USE_WRITEV_FOR_SCREENUPDATES 1

static const int SCREEN_MARGIN_LEFT  = 1;
static const int SCREEN_MARGIN_RIGHT = 0;

/* Callback for updating screen-rectangles */
typedef void (*UpdateRectsFunc)(_THIS, int numrects, SDL_Rect* rects);


/* Initialization/Query functions */
static int SDLONP_VideoInit(_THIS, SDL_PixelFormat* vformat);
static SDL_Rect** SDLONP_ListModes(_THIS, SDL_PixelFormat* format, Uint32 flags);
static SDL_Surface* SDLONP_SetVideoMode(_THIS, SDL_Surface* current, int width, int height, int bpp, Uint32 flags);
static void SDLONP_VideoQuit(_THIS);
static void SDLONP_DeleteDevice(SDL_VideoDevice* device);

/* Framebuffer update functions */
static void SDLONP_UpdateRects(_THIS, int numrects, SDL_Rect* rectangles);
static void SDLONP_UpdateRectsSNES(_THIS, int numrects, SDL_Rect* rectangles);


/* ========== Helper Functions ========== */

static UpdateRectsFunc SDLONP_GetUpdateRectsCallback(struct sdlonp_emuconfig* config)
{
	char* filter = NULL;

	const int rc = sdlonp_emuconfig_get_string(config, SDLONP_EMUCFG_CRT_FILTER, &filter);
	if (rc != SDLONP_EMUCFG_SUCCESS) {
		SDLONP_LOGF("No NTSC-Filter set, using unmodified video-output.");
		return SDLONP_UpdateRects;
	}

	if (SDL_strcmp(filter, SDLONP_FILTER_SNES_NTSC) == 0) {
		SDLONP_LOGF("Using '%s' filter for video-output.", filter);
		return SDLONP_UpdateRectsSNES;
	}

	SDLONP_LOGF("Unknown filter '%s' set for video-output.", filter);
	return NULL;
}


static snes_ntsc_t* SDLONP_GetFilterSNES(struct sdlonp_emuconfig* config)
{
	/* Default preset */
	snes_ntsc_setup_t setup;
	char* preset;

	const int rc = sdlonp_emuconfig_get_string(config, SDLONP_EMUCFG_CRT_PRESET, &preset);
	if ((rc != SDLONP_EMUCFG_SUCCESS) || (SDL_strcmp(preset, SDLONP_FILTER_PRESET_COMPOSITE) == 0)) {
		setup = snes_ntsc_composite;
		preset = SDLONP_FILTER_PRESET_COMPOSITE;
	}
	else if (SDL_strcmp(preset, SDLONP_FILTER_PRESET_MONOCHROME) == 0)
		setup = snes_ntsc_monochrome;
	else if (SDL_strcmp(preset, SDLONP_FILTER_PRESET_SVIDEO) == 0)
		setup = snes_ntsc_svideo;
	else if (SDL_strcmp(preset, SDLONP_FILTER_PRESET_RGB) == 0)
		setup = snes_ntsc_rgb;
	else {
		SDLONP_LOGF("Invalid filter-preset '%s' specified.", preset);
		return NULL;
	}

	setup.merge_fields = 1;

	/* Initialize the filter */
	snes_ntsc_t* filter = (snes_ntsc_t*) SDL_malloc(sizeof(snes_ntsc_t));
	if (filter == NULL) {
		SDLONP_LOGF("Could not allocate memory for snes-ntsc filter.");
		return NULL;
	}

	SDLONP_LOGF("Using filter-preset '%s'.", preset);
	snes_ntsc_init(filter, &setup);
	return filter;
}


static int SDLONP_SendVideoMode(_THIS, SDL_PixelFormat* format, SDLONP_OutputStream* ostream, int locked)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* screen = this->screen;
	if (format == NULL)
		format = screen->format;

	/* Send the pixel-format and size */
	sdlonp_pixel_format pixelfmt;
	pixelfmt.bits_per_pixel = format->BitsPerPixel;
	pixelfmt.bytes_per_pixel = format->BytesPerPixel;
	pixelfmt.red_shift = format->Rshift;
	pixelfmt.green_shift = format->Gshift;
	pixelfmt.blue_shift = format->Bshift;
	pixelfmt.alpha_shift = format->Ashift;
	pixelfmt.red_mask = format->Rmask;
	pixelfmt.green_mask = format->Gmask;
	pixelfmt.blue_mask = format->Bmask;
	pixelfmt.alpha_mask = format->Amask;

	if (locked == 0)
		SDLONP_LockOutputStream(ostream);

	const int retcode = sdlonp_write_pixel_format(ostream->fd, &pixelfmt) +
			sdlonp_write_screen_size(ostream->fd, hidden->client_width, hidden->client_height);

	if (locked == 0)
		SDLONP_UnlockOutputStream(ostream);

	if (retcode != 0)
		SDLONP_LOGF("Could not send new pixel-format and/or screen-size to client!");

	return retcode;
}


static int SDLONP_ConnectClient(const char* sockname)
{
	SDLONP_LOGF("Waiting for connection on '%s'...", sockname);

	const int sfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sfd < 0) {
		SDLONP_LOGF("Creating socket failed! %s.", strerror(errno));
		return -1;
	}

	struct sockaddr_un server_addr;
	memset(&server_addr, 0, sizeof(struct sockaddr_un));
	strncpy(server_addr.sun_path, sockname, sizeof(server_addr.sun_path) - 1);
	server_addr.sun_family = AF_UNIX;

	if (bind(sfd, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_un)) < 0) {
		SDLONP_LOGF("Binding on '%s' failed! %s.", sockname, strerror(errno));
		goto cleanup_on_error;
	}

	if (listen(sfd, 1) < 0) {
		SDLONP_LOGF("Switching to listen mode failed! %s.", strerror(errno));
		goto cleanup_on_error;
	}

	/* Wait for a single client to connect */

	struct sockaddr_un client_addr;
	socklen_t client_addr_size = sizeof(struct sockaddr_un);
	
	int cfd; 
	do {
		cfd = accept(sfd, (struct sockaddr*) &client_addr, &client_addr_size);
	} while(cfd < 0 && errno == EINTR);

	if (cfd < 0) {
		SDLONP_LOGF("Accepting client connection failed! %s.", strerror(errno));
		goto cleanup_on_error;
	}

	SDLONP_LOGF("Client connected successfully on '%s'.", sockname);

	close(sfd);
	return cfd;

	cleanup_on_error: {

		if (cfd > 0)
			close(cfd);

		if (sfd > 0)
			close(sfd);

		return -1;
	}
}


/* ========== SDLONP driver bootstrap functions ========== */

static int SDLONP_Available(void)
{
	const char* envr = SDL_getenv("SDL_VIDEODRIVER");
	if ((envr != NULL) && (SDL_strcmp(envr, SDLONP_VIDEO_DRIVER_NAME) == 0))
		return 1;

	return 0;
}


static SDL_VideoDevice* SDLONP_CreateDevice(int devindex)
{
	SDLONP_LOGF("Initializing SDLONP video-driver...");

	if (SDLONP_InitSharedData() < 0) {
		SDLONP_LOGF("Initializing SDLONP video-driver failed!");
		return NULL;
	}

	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	struct sdlonp_emuconfig* emuconfig = shared_data->emuconfig;

	/* Check, whether the callback for updating the screen was specified */
	UpdateRectsFunc update_rect_callback = SDLONP_GetUpdateRectsCallback(emuconfig);
	if (update_rect_callback == NULL)
		goto cleanup_on_error;

	/* Setup the filter, when filtering was enabled. */
	snes_ntsc_t* filter = NULL;
	if (update_rect_callback != SDLONP_UpdateRects) {
		filter = SDLONP_GetFilterSNES(emuconfig);
		if (filter == NULL)
			goto cleanup_on_error;  /* Something gone wrong */
	}

	/* Create the video-device */
	SDL_VideoDevice* device = (SDL_VideoDevice*) SDL_malloc(sizeof(SDL_VideoDevice));
	if (device == NULL)
		goto cleanup_on_error;

	SDL_memset(device, 0, sizeof(SDL_VideoDevice));

	/* Create the internal video-data */
	SDL_PrivateVideoData* hidden = (SDL_PrivateVideoData*) SDL_malloc(sizeof(SDL_PrivateVideoData));
	if (hidden == NULL)
		goto cleanup_on_error;

	SDL_memset(hidden, 0, sizeof(SDL_PrivateVideoData));
	device->hidden = hidden;

	/* Set the specified filter */
	device->UpdateRects = update_rect_callback;
	device->hidden->ntsc_filter = filter;

	/* Set the function pointers */
	device->VideoInit = SDLONP_VideoInit;
	device->VideoQuit = SDLONP_VideoQuit;
	device->ListModes = SDLONP_ListModes;
	device->SetVideoMode = SDLONP_SetVideoMode;
	device->free = SDLONP_DeleteDevice;

	/* Init input handling */
	if (SDLONP_InitEventHandling(device) < 0) {
		SDLONP_LOGF("Initialization of event-handling subsystem failed!");
		goto cleanup_on_error;
	}

	/* Prepare the input-stream */
	hidden->input_stream = sdlonp_istream_construct(4 * 1024);
	if (hidden->input_stream == NULL)
		goto cleanup_on_error;

	hidden->update_mutex = SDL_CreateMutex();

	/* Signal to the server, that the emulator is ready */
	struct sdlonp_ipc_message* message = &(shared_data->emumsg);
	sdlonp_ipc_compose_notification(message, SDLONP_IPCEID_EMULATOR_READY);
	sdlonp_ipc_sendto(shared_data->emusock, message, &(shared_data->srvsock_address));

	SDLONP_LOGF("SDLONP video-driver was initialized successfully.");

	/* Success */
	return device;

	/* Cleanup */
	cleanup_on_error: {

		if (device != NULL && device->hidden != NULL)
			SDLONP_CleanupEventHandling(device);

		SDLONP_CleanupSharedData();

		if (filter != NULL)
			SDL_free(filter);

		if (hidden != NULL)
			SDL_free(hidden);

		if (device != NULL)
			SDL_free(device);

		SDLONP_LOGF("Initializing SDLONP video-driver failed!");
		return NULL;
	}
}


static void SDLONP_DeleteDevice(SDL_VideoDevice* device)
{
	SDLONP_LOGF("Deleting SDLONP video-driver...");

	SDLONP_DetachClient(device, 0);
	SDLONP_CleanupEventHandling(device);
	SDLONP_CleanupSharedData();

	SDL_PrivateVideoData* hidden = device->hidden;

	if (hidden->input_stream != NULL)
		sdlonp_istream_free(hidden->input_stream);

	SDL_DestroyMutex(hidden->update_mutex);
	SDL_free(hidden->ntsc_filter);
	SDL_free(hidden);
	SDL_free(device);

	SDLONP_LOGF("SDLONP video-driver deleted.");
}


static int SDLONP_GetRequestedBitDepth()
{
	const char* env = SDL_getenv("SDL_BITDEPTH");
	return (env != NULL) ? atoi(env) : 16;
}


int SDLONP_VideoInit(_THIS, SDL_PixelFormat* vformat)
{
	int depth = SDLONP_GetRequestedBitDepth();
	if ((depth != 16) && (depth != 24) && (depth != 32)) {
		SDLONP_LOGF("Requested bitdepth is unsupported: %i. Use defaults.", depth);
		depth = 16;
	}

	vformat->BitsPerPixel = depth;
	vformat->BytesPerPixel = depth / 8;

	return 0;
}


SDL_Rect** SDLONP_ListModes(_THIS, SDL_PixelFormat* format, Uint32 flags)
{
	// This will force SDL lie about what display depth it supports. This
	// defaults to 16 bpp. If set to 0, it will say it supports all depths.
	
	const int depth = SDLONP_GetRequestedBitDepth();
	return (depth == 0 || format->BitsPerPixel == depth) ? (SDL_Rect**) -1 : NULL;
}


SDL_Surface* SDLONP_SetVideoMode(_THIS, SDL_Surface* current, int width, int height, int bpp, Uint32 flags)
{
	SDLONP_LOGF("Setting SDL mode to %dx%d, with %i bits-per-pixel.", width, height, bpp);

	SDL_PrivateVideoData* hidden = this->hidden;
	const int bytes_per_pixel = bpp / 8;

	/* Free old frame-buffer in SDL */
	if (current->pixels != NULL) {
		SDL_free(current->pixels);
		current->pixels = NULL;
	}

	/* Set up the SDL framebuffer */
	current->w = width;
	current->h = height;
	current->flags = flags & SDL_FULLSCREEN;
	current->pitch = current->w * bytes_per_pixel;

	/* Allocate new framebuffer for SDL */
	const size_t framebuf_size = width * height * bytes_per_pixel;
	current->pixels = SDL_malloc(framebuf_size);
	if (current->pixels == NULL) {
		SDL_SetError("Couldn't allocate buffer for SDL framebuffer.");
		return NULL;
	}

	/* Clear the SDL framebuffer */
	SDL_memset(current->pixels, 0, framebuf_size);

	/* Update settings, when NTSC filter was enabled */
	if (this->UpdateRects == SDLONP_UpdateRectsSNES) {
		if (bpp != 16) {
			const char* message = "Video-output filter '%s' expects 16 bpp, but set was %i bpp.";
			SDL_SetError(message, SDLONP_FILTER_SNES_NTSC, bpp);
			return NULL;
		}

		/* Allocate buffer for filter-output */
		const int filtered_width = SNES_NTSC_OUT_WIDTH(width);
		const size_t filtered_framebuf_size = filtered_width * height * bytes_per_pixel;
		void* filtered_pixels = SDL_malloc(filtered_framebuf_size);
		if (filtered_pixels == NULL) {
			SDL_SetError("Couldn't allocate buffer for filtered framebuffer.");
			goto cleanup_on_error;
		}

		/* Clear the pixel buffer */
		SDL_memset(filtered_pixels, 0, filtered_framebuf_size);

		/* Update filtering parameters */
		hidden->filtered_pixels = filtered_pixels;
		hidden->filtered_width = filtered_width;
		hidden->filtered_pitch = filtered_width * bytes_per_pixel;
		hidden->filtered_chunk_count = filtered_width / snes_ntsc_out_chunk;
		hidden->filtered_width_scale = (float) (filtered_width - SCREEN_MARGIN_LEFT - SCREEN_MARGIN_RIGHT) / (float) width;

		/* Prepare masks for scanlines */
		hidden->scanline_masks_count = (height >> 5) + 1;
		hidden->scanline_masks = SDL_malloc(hidden->scanline_masks_count * sizeof(uint32_t));
	}

	hidden->client_width = width;
	hidden->client_height = height;
	hidden->client_pitch = width * bytes_per_pixel;

	SDLONP_LOGF("Setting client-side video-mode to %dx%d, with %i bits-per-pixel.",
			hidden->client_width, hidden->client_height, bpp);

	/* Update the format structure with defaults for specified bpp */
	SDL_PixelFormat* format = SDL_ReallocFormat(current, bpp, 0,0,0,0);
	if (format == NULL) {
		SDL_SetError("Couldn't allocate new pixel format for requested mode");
		goto cleanup_on_error;
	}

	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	SDLONP_OutputStream* ostream = shared_data->output_stream;

	/* Send the pixel-format and size */
	if (SDLONP_IsOutputStreamEnabled(ostream) == 1)
		SDLONP_SendVideoMode(this, current->format, ostream, 0);

	/* Disable mouse cursor overlay */
	SDL_ShowCursor(0);

	/* We're done */
	return current;

	/* Something gone wrong */
	cleanup_on_error: {

		if (current->pixels != NULL)
			SDL_free(current->pixels);

		if (hidden->filtered_pixels != NULL)
			SDL_free(hidden->filtered_pixels);

		current->pixels = NULL;
		hidden->filtered_pixels = NULL;
		return NULL;
	}
}


void SDLONP_VideoQuit(_THIS)
{
	SDLONP_LOGF("Closing SDLONP video-driver...");

	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* sdlscr = this->screen;

	/* Cleanup the framebuffers */

	if (sdlscr->pixels != NULL)
		SDL_free(sdlscr->pixels);

	if (hidden->filtered_pixels != NULL) {
		SDL_free(hidden->filtered_pixels);
		hidden->filtered_pixels = NULL;
	}

	sdlscr->pixels = NULL;

	/* Cleanup the input/output-streams */

	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	struct sdlonp_emuconfig* emuconfig = shared_data->emuconfig;

	char* iosocket;
	const sdlonp_emuconfig_id id = SDLONP_EMUCFG_IOSOCKET;
	if (sdlonp_emuconfig_get_string(emuconfig, id, &iosocket) == SDLONP_EMUCFG_SUCCESS)
		unlink(iosocket);

	SDLONP_LOGF("SDLONP video-driver closed.");
}


int SDLONP_AttachClient(_THIS)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	struct sdlonp_emuconfig* emuconfig = shared_data->emuconfig;

	/* Safety check */
	if (SDLONP_IsOutputStreamEnabled(shared_data->output_stream) == 1) {
		SDLONP_LOGF("An already attached client found! Reattaching not supported.");
		return -1;
	}

	char* sockname = NULL;
	int rc = sdlonp_emuconfig_get_string(emuconfig, SDLONP_EMUCFG_IOSOCKET, &sockname);
	if (rc != SDLONP_EMUCFG_SUCCESS) {
		SDLONP_LOGF("Getting input/output socket name failed! Error code: %i", rc);
		return -1;
	}

	/* Attach client */

	const int client_fd = SDLONP_ConnectClient(sockname);
	if (client_fd < 0)
		return -1;

	SDLONP_LockOutputStream(shared_data->output_stream);

	SDLONP_EnableOutputStream(shared_data->output_stream, client_fd);
	sdlonp_istream_enable(hidden->input_stream, client_fd);
	hidden->client_fd = client_fd;

	if (SDLONP_SendVideoMode(this, NULL, shared_data->output_stream, 1) != 0)
		goto cleanup_on_error;  /* We unlock after cleanup! */

	if (SDLONP_SendAudioFormat(shared_data->output_stream, 1) != 0)
		goto cleanup_on_error;  /* We unlock after cleanup! */

	/* Send a fullscreen-update */
	SDL_Surface* screen = this->screen;
	SDL_Rect rectangle = {
			.x = 0,
			.y = 0,
			.w = screen->w,
			.h = screen->h
	};

	this->UpdateRects(this, 1, &rectangle);

	SDLONP_UnlockOutputStream(shared_data->output_stream);

	/* Signal to the server, that this operation is done */
	struct sdlonp_ipc_message* message = &(shared_data->emumsg);
	sdlonp_ipc_compose_notification(message, SDLONP_IPCEID_CLIENT_ATTACHED);
	sdlonp_ipc_sendto(shared_data->emusock, message, &(shared_data->srvsock_address));

	return 0;

	cleanup_on_error: {

		/* Stream is locked here, cleanup and unlock. */

		if (client_fd > 0)
			close(client_fd);

		hidden->client_fd = -1;

		sdlonp_istream_disable(hidden->input_stream);
		SDLONP_DisableOutputStream(shared_data->output_stream);
		SDLONP_UnlockOutputStream(shared_data->output_stream);

		return -1;
	}
}


int SDLONP_DetachClient(_THIS, int notify)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();

	if (SDLONP_IsOutputStreamEnabled(shared_data->output_stream) == 0)
		return 0;  /* No client attached, nothing to do! */

	SDLONP_LockOutputStream(shared_data->output_stream);

	if (hidden->client_fd > 0)
		close(hidden->client_fd);

	hidden->client_fd = -1;

	sdlonp_istream_disable(hidden->input_stream);
	SDLONP_DisableOutputStream(shared_data->output_stream);

	SDLONP_UnlockOutputStream(shared_data->output_stream);

	if (notify == 1) {
		/* Signal to the server, that this operation is done */
		struct sdlonp_ipc_message* message = &(shared_data->emumsg);
		sdlonp_ipc_compose_notification(message, SDLONP_IPCEID_CLIENT_DETACHED);
		sdlonp_ipc_sendto(shared_data->emusock, message, &(shared_data->srvsock_address));
	}

	SDLONP_LOGF("Client detached.");

	return 0;
}


/* =============== Drawing callbacks =============== */

/* Interpolate a single color component, from 4 packed pixels */
static inline uint16_t SDLONP_InterpolateCubic(uint16_t shift, uint16_t mask, float mu, const uint16_t* comps)
{
	const int y[4] = {
			(int) ((comps[0] >> shift) & mask),
			(int) ((comps[1] >> shift) & mask),
			(int) ((comps[2] >> shift) & mask),
			(int) ((comps[3] >> shift) & mask)
	};

	const float a0 = (float) (y[3] - y[2] - y[0] + y[1]);
	const float a1 = (float) (y[0] - y[1] - a0);
	const float a2 = (float) (y[2] - y[0]);
	const float a3 = (float) y[1];
	const float mu2 = mu * mu;

	int16_t component = (int16_t) (a0*mu*mu2 + a1*mu2 + a2*mu + a3);
	if (component > mask)
		component = mask;
	else if (component < 0)
		component = 0;

	return component;
}


/* Interpolate a single packed pixel, using Cubic-Filter */
static unsigned short SDLONP_InterpolateCubicRGB(const uint16_t* comps, float mu)
{
	const uint16_t SHIFT_R = 11;
	const uint16_t SHIFT_G = 5;
	const uint16_t SHIFT_B = 0;

	const uint16_t MASK_R = 0x1F;
	const uint16_t MASK_G = 0x3F;
	const uint16_t MASK_B = 0x1F;

	uint16_t pixel = 0;

	pixel  = SDLONP_InterpolateCubic(SHIFT_R, MASK_R, mu, comps) << SHIFT_R;
	pixel |= SDLONP_InterpolateCubic(SHIFT_G, MASK_G, mu, comps) << SHIFT_G;
	pixel |= SDLONP_InterpolateCubic(SHIFT_B, MASK_B, mu, comps);

	return pixel;
}


/* Downsample pixels using Cubic-Interpolation */
static void SDLONP_DownsampleCubic(SDL_Rect* rect, float scaling, int input_width, int max_output_width,
		int input_pitch, int output_pitch, const uint8_t* input_pixels, uint8_t* output_pixels)
{
	int i, j;

	const int skip_first = (rect->x == 0) ? 1 : 0;
	const int skip_last  = (rect->x + rect->w < max_output_width) ? 0 : 1;
	const int dx = rect->w - skip_first - skip_last;
	const float srcx_f_start = ((float) (rect->x + skip_first + SCREEN_MARGIN_LEFT)) * scaling;

	input_pixels += rect->y * input_pitch - sizeof(uint16_t);

	for (i = rect->h; i > 0; --i) {

		const uint16_t* input = (const uint16_t*) input_pixels;
		uint16_t* output = (uint16_t*) output_pixels;

		/* Special case: scanline's first pixel */
		if (skip_first != 0) {
			*output = *(input + 1 + SCREEN_MARGIN_LEFT);
			++output;
		}

		float srcx_f = srcx_f_start;

		/* Handle the rest of pixels, excluding last pixel */
		for (j = dx; j > 0; --j) {

			const int srcx_i = (int) srcx_f;
			const float mu = srcx_f - (float) srcx_i;
			srcx_f += scaling;  /* advance to next position */

			*output = SDLONP_InterpolateCubicRGB(input + srcx_i, mu);
			++output;
		}

		/* Special case: scanline's last pixel */
		if (skip_last != 0)
			*output = *(input + input_width - SCREEN_MARGIN_RIGHT);

		output_pixels += output_pitch;
		input_pixels += input_pitch;
	}
}


static void SDLONP_PrepareScreenRectBuffer(_THIS, SDL_Rect* rect)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* screen = this->screen;

	/* Update's length in bytes */
	const int bytes_per_pixel = screen->format->BytesPerPixel;
	const int length = rect->w * rect->h * bytes_per_pixel;
	hidden->update_buffer_length = length;

	/* Is the buffer big enough for this screen-update? */
	if (length > hidden->update_buffer_capacity) {
		SDL_free(hidden->update_buffer);

		/* Allocate a bigger buffer */
		hidden->update_buffer = SDL_malloc(length);
		hidden->update_buffer_capacity = length;
	}
}


static int SDLONP_SendScreenRect(_THIS, SDL_Rect* rect)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* screen = this->screen;
	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	SDLONP_OutputStream* ostream = shared_data->output_stream;
	sdlonp_screen_update_header header;

	/* Set header parameters */
	header.xpos   = rect->x;
	header.ypos   = rect->y;
	header.width  = rect->w;
	header.height = rect->h;

	header.buffer_length = hidden->update_buffer_length;

	SDLONP_LockOutputStream(ostream);

	/* Client already disconnected? */
	if (SDLONP_IsOutputStreamEnabled(ostream) == 0) {
		SDLONP_UnlockOutputStream(ostream);
		return -1;  /* Yes */
	}

	/* Send the passed buffer to client */
	const int result = sdlonp_write_screen_update(ostream->fd, &header, hidden->update_buffer);
	SDLONP_UnlockOutputStream(ostream);
	if (result == 0)
		return 0;

	SDLONP_LOGF("Sending screen-rectangle failed!");
	SDLONP_DetachClient(this, 1);
	return -1;
}


static void SDLONP_CopyScreenRect(_THIS, SDL_Rect* rect)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* screen = this->screen;

	int8_t const* pixels = screen->pixels;
	const int pitch = screen->pitch;

	const int bytes_per_pixel = screen->format->BytesPerPixel;
	const size_t length = rect->w * bytes_per_pixel;
	const size_t xoffset = rect->x * bytes_per_pixel;
	const char* srcline = pixels + (rect->y * pitch) + xoffset;
	char* dstline = hidden->update_buffer;
	int i = rect->h;

	/* Copy the screen-rectangle */
	while (i > 0) {
		SDL_memcpy(dstline, srcline, length);
		srcline += pitch;
		dstline += length;
		--i;
	}
}


static void SDLONP_UpdateRects(_THIS, int numrects, SDL_Rect* rectangles)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* screen = this->screen;
	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	SDLONP_OutputStream* ostream = shared_data->output_stream;

	/* Protect shared resources here, since some
	 * emulators call this callback concurrently. */

	SDL_LockMutex(hidden->update_mutex);

#ifdef SDLONP_USE_WRITEV_FOR_SCREENUPDATES

	sdlonp_screen_update_header header;

	const int bytes_per_pixel = screen->format->BytesPerPixel;
	const int pitch = screen->pitch;
	uint8_t* pixels = screen->pixels;

	for (int i = 0; i < numrects; ++i) {
		SDL_Rect* rect = &rectangles[i];

		/* Set header parameters */
		header.xpos   = rect->x;
		header.ypos   = rect->y;
		header.width  = rect->w;
		header.height = rect->h;

		/* Update's length in bytes */
		const int length = rect->w * bytes_per_pixel;
		header.buffer_length = length * rect->h;

		const size_t xoffset = rect->x * bytes_per_pixel;
		void* srcline = pixels + (rect->y * pitch) + xoffset;

		SDLONP_LockOutputStream(ostream);

		/* Client already disconnected? */
		if (SDLONP_IsOutputStreamEnabled(ostream) == 0) {
			SDLONP_UnlockOutputStream(ostream);
			break;  /* Yes */
		}

		/* Send the passed buffer to client */
		if (sdlonp_writev_screen_update(ostream->fd, &header, srcline, length, pitch) != 0) {
			SDLONP_LOGF("Sending screen-rectangle failed!");
			SDLONP_UnlockOutputStream(ostream);
			SDLONP_DetachClient(this, 1);
			break;
		}

		SDLONP_UnlockOutputStream(ostream);
	}

#else

	for (int i = 0; i < numrects; ++i) {
		SDL_Rect* rect = &rectangles[i];
		SDLONP_PrepareScreenRectBuffer(this, rect);
		SDLONP_CopyScreenRect(this, rect);
		if (SDLONP_SendScreenRect(this, rect) < 0)
			break;  /* Client is disconnected! */
	}

#endif  /* SDLONP_USE_WRITEV_FOR_SCREENUPDATES */

	SDL_UnlockMutex(hidden->update_mutex);
}


static void SDLONP_UpdateRectsSNES(_THIS, int numrects, SDL_Rect* rectangles)
{
	static const Sint16 XOFFSET = 3;
	static const Uint16 WOFFSET = XOFFSET << 1;
	static const uint32_t BITMASK_SHIFT = 5;
	static const uint32_t BITMASK_WIDTH = 1 << BITMASK_SHIFT;

	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* screen = this->screen;
	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	SDLONP_OutputStream* ostream = shared_data->output_stream;

	/* Protect shared resources here, since some
	 * emulators call this callback concurrently. */
	SDL_LockMutex(hidden->update_mutex);

	const int client_width = hidden->client_width;
	const int client_pitch = hidden->client_pitch;
	const int filtered_pitch = hidden->filtered_pitch;
	const uint8_t* input_pixels = screen->pixels;
	uint8_t* filtered_pixels = hidden->filtered_pixels;
	uint32_t* scanline_masks = hidden->scanline_masks;

	int i, j;

	/* Clear all scanline masks */
	for (i = 0; i < hidden->scanline_masks_count; ++i)
		scanline_masks[i] = 0x0;

	/* Process the updated rectangles... */
	for (i = 0; i < numrects; ++i) {

		SDL_Rect* rect = &rectangles[i];
		uint32_t remaining = rect->h;
		uint32_t ycur = rect->y;
		uint32_t height = 0;

		/* Filter only unfiltered scanlines... */
		while (remaining > 0) {

			/* Find a block of contiguous unfiltered scanlines */
			const uint32_t scanline_mask_index = ycur >> BITMASK_SHIFT;
			const uint32_t bit_number = ycur - (scanline_mask_index << BITMASK_SHIFT);
			const uint32_t bit_number_last = bit_number + remaining;
			const uint32_t bitmask_last = (bit_number_last < BITMASK_WIDTH) ? 1 << bit_number_last : 0x0;
			uint32_t bitmask = 1 << bit_number;
			do {
				if ((scanline_masks[scanline_mask_index] & bitmask) != 0)
					break;  /* Filtered scanline found */

				scanline_masks[scanline_mask_index] |= bitmask;

				/* Check next scanline */
				bitmask <<= 1;
				++height;
			}
			while (bitmask != bitmask_last);

			/* Unfiltered scanlines found? */
			if (height > 0) {
				const int input_offset = ycur * screen->pitch;
				const int filtered_offset = ycur * filtered_pitch;

				/* Filter the current block of scanlines */
				snes_ntsc_blit(hidden->ntsc_filter, (const uint16_t*) (input_pixels + input_offset),
						screen->w, 0, screen->w, height, filtered_pixels + filtered_offset, filtered_pitch);

				remaining -= height;
				ycur += height;
				height = 0;
			}
			else {
				--remaining;
				++ycur;
			}
		}

		/* Adjust the rectangle for filtered output */

		if (rect->x > XOFFSET)
			rect->x -= XOFFSET;
		else rect->x = 0;

		rect->w += WOFFSET;
		if ((rect->x + rect->w) >= client_width)
			rect->w = client_width - rect->x;

		/* Downsample the filtered scanlines and send the rectangle to client */

		SDLONP_PrepareScreenRectBuffer(this, rect);

		const int output_pitch = rect->w * screen->format->BytesPerPixel;
		SDLONP_DownsampleCubic(rect, hidden->filtered_width_scale, hidden->filtered_width,
				client_width, filtered_pitch, output_pitch, filtered_pixels, hidden->update_buffer);

		if (SDLONP_SendScreenRect(this, rect) < 0)
			break;  /* Client is disconnected! */
	}

	SDL_UnlockMutex(hidden->update_mutex);
}


VideoBootStrap SDLONP_bootstrap = {
	SDLONP_VIDEO_DRIVER_NAME, "SDL-Over-NamedPipe video-driver (with ntsc-filter support)",
	SDLONP_Available, SDLONP_CreateDevice
};
