#ifndef __SDLONP_KEYCODE_INDEX_H
#define __SDLONP_KEYCODE_INDEX_H

#include <xkbcommon/xkbcommon.h>

/** A lookup table: keysym -> keycode */
struct sdlonp_keycode_index;

/** Creates a new lookup table instance. */
struct sdlonp_keycode_index* sdlonp_keycode_index_new(uint32_t num_buckets);

/** Destroys the specified lookup table instance. */
void sdlonp_keycode_index_free(struct sdlonp_keycode_index* index);

/** Populates the lookup table with data from the specified keymap. */
void sdlonp_keycode_index_build(struct sdlonp_keycode_index* index, struct xkb_keymap* keymap);

/**
 * Lookup the corresponding keycode for the specified keysym.
 * @return A valid keycode if the keysym is found, else XKB_KEYCODE_INVALID.
 */
xkb_keycode_t sdlonp_keycode_index_lookup(struct sdlonp_keycode_index* index, xkb_keysym_t keysym);

#endif /* __SDLONP_KEYCODE_INDEX_H */
