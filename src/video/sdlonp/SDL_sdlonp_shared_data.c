#include "SDL_sdlonp_shared_data.h"
#include "SDL_sdlonp_logging.h"

/* Environment variables */
#define SDLONP_ENV_EMUCTLSOCKET  "SDL_EMUCTLSOCKET"
#define SDLONP_ENV_SRVCTLSOCKET  "SDL_SRVCTLSOCKET"

static int shared_data_numrefs = 0;

static SDLONP_SharedData shared_data = {
		.emuconfig = NULL,
		.output_stream = NULL,
		.emusock = NULL,
};


static char* SDLONP_GetStringVar(char* envname)
{
	/* TODO: read it from file! */
	char* value = SDL_getenv(envname);
	if (value == NULL) {
		SDLONP_LOGF("%s was not set!", envname);
		return NULL;
	}

	return value;
}


static struct sdlonp_emuconfig* SDLONP_ReceiveEmuConfig(struct sdlonp_ipc_socket* socket)
{
	struct sdlonp_emuconfig* emuconfig = sdlonp_emuconfig_new();
	if (emuconfig == NULL)
		return NULL;

	struct sdlonp_ipc_message message;
	sdlonp_ipc_init_message(socket, &message);

	for (int i = 0; i < 25; ++i) {
		if (sdlonp_ipc_timedreceive(socket, &message, 15000) != SDLONP_IPCRES_SUCCESS){
			SDLONP_LOGF("Receiving emulator's configuration failed!");
			goto cleanup_on_error;
		}

		if (message.type == SDLONP_IPCMSG_CONFIG_ENTRY)
			sdlonp_emuconfig_parse_value(emuconfig, message.data, message.length);

		else if (message.type == SDLONP_IPCMSG_CONFIG_UPDATE)
			break;  /* All entries received! */
	}

	sdlonp_ipc_release_message(&message);
	return emuconfig;

	cleanup_on_error: {
		sdlonp_ipc_release_message(&message);
		sdlonp_emuconfig_free(emuconfig);
		return NULL;
	}
}


int SDLONP_InitSharedData()
{
	if (shared_data.emuconfig != NULL) {
		++shared_data_numrefs;
		return 0;
	}

	char* emusock_name = SDLONP_GetStringVar(SDLONP_ENV_EMUCTLSOCKET);
	char* srvsock_name = SDLONP_GetStringVar(SDLONP_ENV_SRVCTLSOCKET);
	if (emusock_name == NULL || srvsock_name == NULL)
		return 0;

	struct sdlonp_ipc_socket* emusock = sdlonp_ipc_create(emusock_name, SOCK_DGRAM);
	if (emusock == NULL)
		return 0;

	if (sdlonp_ipc_init_message(emusock, &(shared_data.emumsg)) != SDLONP_IPCRES_SUCCESS)
		goto cleanup_on_error;

	sdlonp_ipc_init_address(&(shared_data.srvsock_address), srvsock_name);

	/* Signal to the server, that the control-socket is ready */
	struct sdlonp_ipc_message* message = &(shared_data.emumsg);
	sdlonp_ipc_compose_notification(message, SDLONP_IPCEID_EMULATOR_CTLSOCK_READY);
	sdlonp_ipc_sendto(emusock, message, &(shared_data.srvsock_address));

	/* Setup emulator's configuration */
	struct sdlonp_emuconfig* emuconfig = SDLONP_ReceiveEmuConfig(emusock);
	if (emuconfig == NULL)
		goto cleanup_on_error;

	if (!sdlonp_emuconfig_is_valid(emuconfig, SDLONP_EMUCFG_IOSOCKET)) {
		SDLONP_LOGF("Name for the input/output socket was not set!");
		goto cleanup_on_error;
	}

	sdlonp_ipc_enable_nonblocking_mode(emusock);

	/* Prepare the output-stream here */
	SDLONP_OutputStream* output_stream = SDLONP_CreateOutputStream();
	if (output_stream == NULL)
		goto cleanup_on_error;

	/* Update shared data */
	shared_data.emuconfig = emuconfig;
	shared_data.output_stream = output_stream;
	shared_data.emusock = emusock;
	++shared_data_numrefs;

	SDLONP_LOGF("Shared data initialized successfully.");

	return 1;

	cleanup_on_error: {

		if (output_stream != NULL)
			SDLONP_DestroyOutputStream(output_stream);

		if (emuconfig != NULL)
			sdlonp_emuconfig_free(emuconfig);

		sdlonp_ipc_release_message(&(shared_data.emumsg));

		if (emusock != NULL)
			sdlonp_ipc_free(emusock);

		shared_data.emuconfig = NULL;
		shared_data.output_stream = NULL;
		shared_data.emusock = NULL;

		return -1;
	}
}


void SDLONP_CleanupSharedData()
{
	if (shared_data.emuconfig == NULL)
		return;  /* Nothing to do! */

	if (--shared_data_numrefs > 0)
		return;  /* References left, defer cleanup */

	if (shared_data.output_stream != NULL)
		SDLONP_DestroyOutputStream(shared_data.output_stream);

	if (shared_data.emuconfig != NULL)
		sdlonp_emuconfig_free(shared_data.emuconfig);

	if (shared_data.emusock != NULL)
		sdlonp_ipc_free(shared_data.emusock);

	sdlonp_ipc_release_message(&(shared_data.emumsg));

	/* Clear struct */
	shared_data.emuconfig = NULL;
	shared_data.output_stream = NULL;
	shared_data.emusock = NULL;

	SDLONP_LOGF("Shared data cleaned up.");
}


SDLONP_SharedData* SDLONP_GetSharedData()
{
	return (shared_data.emuconfig != NULL) ? &shared_data : NULL;
}
