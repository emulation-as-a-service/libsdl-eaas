#ifndef __SDL_SDLONP_SHARED_DATA_H
#define __SDL_SDLONP_SHARED_DATA_H

#include "SDL_sdlonp_ostream.h"
#include "SDL_sdlonp_emuconfig.h"
#include "SDL_sdlonp_ipc.h"

struct SDLONP_SharedData
{
	struct sdlonp_emuconfig* emuconfig;
	SDLONP_OutputStream* output_stream;
	struct sockaddr_un srvsock_address;
	struct sdlonp_ipc_socket* emusock;
	struct sdlonp_ipc_message emumsg;
};

typedef struct SDLONP_SharedData SDLONP_SharedData;

int SDLONP_InitSharedData();
void SDLONP_CleanupSharedData();

SDLONP_SharedData* SDLONP_GetSharedData();

#endif /* __SDL_SDLONP_SHARED_DATA_H */
