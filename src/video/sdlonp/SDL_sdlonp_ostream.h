
#ifndef __SDLONP_OSTREAM_H
#define __SDLONP_OSTREAM_H

#include "SDL_config.h"
#include "SDL_thread.h"

struct SDLONP_OutputStream
{
	SDL_mutex* mutex;
	int fd;
};

typedef struct SDLONP_OutputStream SDLONP_OutputStream;


SDLONP_OutputStream* SDLONP_CreateOutputStream();
void SDLONP_DestroyOutputStream(SDLONP_OutputStream* ostream);


inline void SDLONP_LockOutputStream(SDLONP_OutputStream* ostream)
{
	SDL_LockMutex(ostream->mutex);
}

inline void SDLONP_UnlockOutputStream(SDLONP_OutputStream* ostream)
{
	SDL_UnlockMutex(ostream->mutex);
}

inline void SDLONP_EnableOutputStream(SDLONP_OutputStream* ostream, int fd)
{
	ostream->fd = fd;
}

inline void SDLONP_DisableOutputStream(SDLONP_OutputStream* ostream)
{
	ostream->fd = -1;
}

inline int SDLONP_IsOutputStreamEnabled(SDLONP_OutputStream* ostream)
{
	return ((ostream != NULL) && (ostream->fd > 0)) ? 1 : 0;
}

#endif /* __SDLONP_OSTREAM_H */
