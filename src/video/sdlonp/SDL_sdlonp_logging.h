#ifndef __SDLONP_HELPERS_H
#define __SDLONP_HELPERS_H

#include <stdio.h>
#include <syslog.h>
#include <time.h>

/* Logging Helpers */
#define SDLONP_LOGF(_frmt_, ...) \
	{  	struct timespec time;  \
		clock_gettime(CLOCK_MONOTONIC, &time);  \
		printf("%ld.%ld [SDLONP-VIDEO] "_frmt_"\n", time.tv_sec, time.tv_nsec, ##__VA_ARGS__); \
		syslog(LOG_USER | LOG_INFO, "[SDLONP-VIDEO] "_frmt_, ##__VA_ARGS__); }

#ifdef SDLONP_ENABLE_DEBUG_MESSAGES
#	define SDLONP_DEBUGF(_frmt_, ...)  SDLONP_LOGF(_frmt_, __VA_ARGS__)
#else
#	define SDLONP_DEBUGF(_frmt_, ...)  { }
#endif /* SDLONP_ENABLE_DEBUG_MESSAGES */

#endif /* __SDLONP_HELPERS_H */
