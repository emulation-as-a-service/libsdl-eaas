#include "SDL_sdlonp_logging.h"
#include "SDL_sdlonp_ipc.h"
#include <poll.h>
#include <unistd.h>
#include <errno.h>

/* Number of bytes for message type */
#define MSGTYPE_SIZE 1

/* Default size of message-buffers for IPC */
#define DEFAULT_MSGBUFFER_CAPACITY 8120

#define SDLONP_CAST(type, value)  *((type*) &(value))


sdlonp_ipc_result sdlonp_ipc_init_message(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message)
{
	message->buffer.data = SDL_malloc(socket->max_msgsize);
	message->buffer.capacity = socket->max_msgsize;
	message->type = SDLONP_IPCMSG_INVALID;
	message->data = message->buffer.data + MSGTYPE_SIZE;
	message->capacity = message->buffer.capacity - MSGTYPE_SIZE;
	message->length = 0;

	return SDLONP_IPCRES_SUCCESS;
}


void sdlonp_ipc_release_message(struct sdlonp_ipc_message* message)
{
	struct sdlonp_ipc_buffer* buffer = &(message->buffer);
	if (buffer->data != NULL)
		SDL_free(buffer->data);

	message->buffer.data = NULL;
	message->buffer.capacity = 0;
	message->type = SDLONP_IPCMSG_INVALID;
	message->data = NULL;
	message->capacity = 0;
	message->length = 0;
}


void sdlonp_ipc_compose_notification(struct sdlonp_ipc_message* message, sdlonp_ipc_eventid eventid)
{
	message->type = SDLONP_IPCMSG_NOTIFICATION;
	message->data[0] = eventid;
	message->length = 1;
}


void sdlonp_ipc_init_address(struct sockaddr_un* address, char* sockpath)
{
	memset(address, 0, sizeof(struct sockaddr_un));
	strncpy(address->sun_path, sockpath, sizeof(address->sun_path) - 1);
	address->sun_family = AF_UNIX;
}


struct sdlonp_ipc_socket* sdlonp_ipc_create(char* sockname, int socktype)
{
	struct sdlonp_ipc_socket* sock = SDL_malloc(sizeof (struct sdlonp_ipc_socket));
	if (socket == NULL) {
		SDLONP_LOGF("Allocating socket handle for IPC failed!");
		return NULL;
	}

	const int sockfd = socket(AF_UNIX, socktype, 0);
	if (sockfd < 0) {
		SDLONP_LOGF("Creating socket '%s' failed! %s", sockname, strerror(errno));
		return NULL;
	}

	int sndbuf_capacity = 0;
	int rcvbuf_capacity = 0;

	/* Lookup the send-buffer's capacity */
	int optlen = sizeof(sndbuf_capacity);
	if (getsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &sndbuf_capacity, &optlen) < 0)
		sndbuf_capacity = DEFAULT_MSGBUFFER_CAPACITY;

	/* Lookup the receive-buffer's capacity */
	optlen = sizeof(rcvbuf_capacity);
	if (getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf_capacity, &optlen) < 0)
		rcvbuf_capacity = DEFAULT_MSGBUFFER_CAPACITY;

	struct sockaddr_un addr;
	memset(&addr, 0, sizeof(struct sockaddr_un));
	const int pathlength = sizeof(addr.sun_path) - 1;
	strncpy(addr.sun_path, sockname, pathlength);
	addr.sun_family = AF_UNIX;

	if (bind(sockfd, &addr, sizeof(struct sockaddr_un)) < 0) {
		SDLONP_LOGF("Binding socket on '%s' failed! %s.", sockname, strerror(errno));
		close(sockfd);
		return NULL;
	}

	sock->fd = sockfd;
	sock->flags = 0;
	sock->max_msgsize = (sndbuf_capacity < rcvbuf_capacity) ? sndbuf_capacity : rcvbuf_capacity;

	const int namelength = strlen(sockname);
	sock->address = SDL_malloc(namelength + 1);
	strncpy(sock->address, sockname, namelength);
	sock->address[namelength] = '\0';

	SDLONP_LOGF("IPC socket '%s' created.", sockname);

	return sock;
}


void sdlonp_ipc_free(struct sdlonp_ipc_socket* socket)
{
	if (socket->fd > 0)
		close(socket->fd);

	if (socket->address != NULL) {
		unlink(socket->address);
		SDL_free(socket->address);
	}

	SDL_free(socket);
}


sdlonp_ipc_result sdlonp_ipc_receive(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message)
{
	struct sdlonp_ipc_buffer* buffer = &(message->buffer);
	ssize_t numbytes = 0;

	retry_recvfrom:

	numbytes = recvfrom(socket->fd, buffer->data, buffer->capacity, socket->flags, NULL, NULL);
	if (numbytes < 0) {
		/* Non-blocking mode? */
		if (errno == EAGAIN)
			return SDLONP_IPCRES_RETRY;

		/* Call interrupted? */
		if (errno == EINTR)
			goto retry_recvfrom;

		SDLONP_LOGF("Receiving message from socket '%s' failed! %s", socket->address, strerror(errno));
		return SDLONP_IPCRES_ERROR;
	}

	message->type = *((uint8_t*) buffer->data);
	message->length = numbytes - 1;
	return SDLONP_IPCRES_SUCCESS;
}


sdlonp_ipc_result sdlonp_ipc_timedreceive(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message, int timeout)
{
	struct pollfd fds = {
			.fd = socket->fd,
			.events = POLLIN,
			.revents = 0
	};

	int numevents = 0;

	retry_poll:

	/* Wait for data to become available */
	numevents = poll(&fds, 1, timeout);
	if (numevents < 0) {
		/* Call interrupted? */
		if (errno == EINTR)
			goto retry_poll;

		SDLONP_LOGF("Polling socket '%s' failed! %s", socket->address, strerror(errno));
		return SDLONP_IPCRES_ERROR;
	}
	if (numevents == 0) {
		SDLONP_LOGF("Polling socket '%s' timedout!", socket->address);
		return SDLONP_IPCRES_ERROR;
	}

	return sdlonp_ipc_receive(socket, message);
}


sdlonp_ipc_result sdlonp_ipc_sendto(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message, struct sockaddr_un* dstaddr)
{
	struct sdlonp_ipc_buffer* buffer = &(message->buffer);
	buffer->data[0] = SDLONP_CAST(char, message->type);

	retry_sendto:

	if (sendto(socket->fd, buffer->data, message->length + MSGTYPE_SIZE, socket->flags, dstaddr, sizeof(*dstaddr)) >= 0)
		return SDLONP_IPCRES_SUCCESS;

	/* Non-blocking mode? */
	if (errno == EAGAIN)
		return SDLONP_IPCRES_RETRY;

	/* Call interrupted? */
	if (errno == EINTR)
		goto retry_sendto;

	SDLONP_LOGF("Sending message to socket '%s' failed! %s", dstaddr->sun_path, strerror(errno));
	return SDLONP_IPCRES_ERROR;
}


void sdlonp_ipc_enable_nonblocking_mode(struct sdlonp_ipc_socket* socket)
{
	socket->flags |= MSG_DONTWAIT;
}


void sdlonp_ipc_disable_nonblocking_mode(struct sdlonp_ipc_socket* socket)
{
	socket->flags &= ~MSG_DONTWAIT;
}
