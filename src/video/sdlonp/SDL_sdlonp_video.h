/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#include "SDL_config.h"

#ifndef _SDL_sdlonp_video_h
#define _SDL_sdlonp_video_h

#include "SDL_thread.h"
#include "../SDL_sysvideo.h"
#include "ntsc-filter/snes_ntsc.h"
#include "sdlonp_keycode_index.h"
#include <guacamole/sdlonp/protocol.h>
#include <guacamole/sdlonp/stream.h>
#include <xkbcommon/xkbcommon.h>

/* Hidden "this" pointer for the video functions */
#define _THIS  SDL_VideoDevice* this

/* Private display data */
struct SDL_PrivateVideoData
{
	int client_fd;
	sdlonp_istream* input_stream;

	/* Client's screen properties */
	int client_width;
	int client_height;
	int client_pitch;

	/* Filter */
	snes_ntsc_t* ntsc_filter;
	void* filtered_pixels;
	int filtered_width;
	int filtered_pitch;
	int filtered_chunk_count;
	float filtered_width_scale;
	uint32_t* scanline_masks;
	int scanline_masks_count;

	/* Buffer for screen-updates */
	SDL_mutex* update_mutex;
	char* update_buffer;
	int update_buffer_length;
	int update_buffer_capacity;

	/* Events */
	struct xkb_context* keyboard_context;
	struct xkb_state* keyboard_state;
	struct sdlonp_keycode_index* keycode_index;
	sdlonp_mouse_event* pending_mouse_event;
	Uint32 last_mouse_timestamp;
	Uint32 last_input_timestamp;
	Uint32 inactivity_timeout;
	int last_button_state;
	int relative_mouse;
	int hard_termination;
};

typedef struct SDL_PrivateVideoData SDL_PrivateVideoData;

int SDLONP_AttachClient(_THIS);
int SDLONP_DetachClient(_THIS, int notify);

#endif /* _SDL_sdlonp_video_h */
