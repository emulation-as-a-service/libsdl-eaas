#include "SDL_sdlonp_emuconfig.h"
#include "SDL_error.h"


enum sdlonp_emuconfig_constants
{
	SDLONP_EMUCFG_NUM_INT8_ENTRIES    = 2,
	SDLONP_EMUCFG_NUM_INT32_ENTRIES   = 1,
	SDLONP_EMUCFG_NUM_FLOAT_ENTRIES   = 0,
	SDLONP_EMUCFG_NUM_STRING_ENTRIES  = 7,

	SDLONP_EMUCFG_TYPE_SHIFT  = 6,
	SDLONP_EMUCFG_TYPE_MASK   = 3 << SDLONP_EMUCFG_TYPE_SHIFT,
	SDLONP_EMUCFG_INDEX_MASK  = 0xFF ^ SDLONP_EMUCFG_TYPE_MASK,
};


struct sdlonp_emuconfig
{
	/* Entries */
	int8_t int8_entries[SDLONP_EMUCFG_NUM_INT8_ENTRIES];
	int32_t int32_entries[SDLONP_EMUCFG_NUM_INT32_ENTRIES];
	float float_entries[SDLONP_EMUCFG_NUM_FLOAT_ENTRIES];
	char* string_entries[SDLONP_EMUCFG_NUM_STRING_ENTRIES];

	uint32_t dirtymasks[4];
	uint32_t validmasks[4];
};


union sdlonp_value
{
	float    f32;
	uint32_t u32;
	int32_t  s32;
	uint8_t  u8[4];
	int8_t   s8[4];
};


/* ================= Internal Helpers ================== */

inline static int sdlonp_emuconfig_id_to_index(int id)
{
	return (id & SDLONP_EMUCFG_INDEX_MASK);
}


inline static int sdlonp_emuconfig_id_to_type(int id)
{
	return (id & SDLONP_EMUCFG_TYPE_MASK);
}


static void sdlonp_emuconfig_mark_as_changed(struct sdlonp_emuconfig* config, int type, int index)
{
	type >>= SDLONP_EMUCFG_TYPE_SHIFT;
	const uint32_t flag = 1 << index;
	config->dirtymasks[type] |= flag;
	config->validmasks[type] |= flag;
}


static void sdlonp_emuconfig_mark_as_unchanged(struct sdlonp_emuconfig* config, int type, int index)
{
	type >>= SDLONP_EMUCFG_TYPE_SHIFT;
	const uint32_t mask = ~(1 << index);
	config->dirtymasks[type] &= mask;
}


static sdlonp_emuconfig_result sdlonp_emuconfig_set_int8(struct sdlonp_emuconfig* config, int index, int8_t value)
{
	if (index >= SDLONP_EMUCFG_NUM_INT8_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_changed(config, SDLONP_EMUCFG_INT8, index);

	config->int8_entries[index] = value;
	return SDLONP_EMUCFG_SUCCESS;
}


static sdlonp_emuconfig_result sdlonp_emuconfig_set_int32(struct sdlonp_emuconfig* config, int index, int32_t value)
{
	if (index >= SDLONP_EMUCFG_NUM_INT32_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_changed(config, SDLONP_EMUCFG_INT32, index);

	config->int32_entries[index] = value;
	return SDLONP_EMUCFG_SUCCESS;
}


static sdlonp_emuconfig_result sdlonp_emuconfig_set_float(struct sdlonp_emuconfig* config, int index, float value)
{
	if (index >= SDLONP_EMUCFG_NUM_FLOAT_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_changed(config, SDLONP_EMUCFG_FLOAT, index);

	config->float_entries[index] = value;
	return SDLONP_EMUCFG_SUCCESS;
}


/* Set a new string entry. The string is expected to be null-terminated! */
static sdlonp_emuconfig_result sdlonp_emuconfig_set_string(struct sdlonp_emuconfig* config, int index, char* value)
{
	if (index >= SDLONP_EMUCFG_NUM_STRING_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	char* oldvalue = config->string_entries[index];
	if (oldvalue != NULL)
		SDL_free(oldvalue);

	sdlonp_emuconfig_mark_as_changed(config, SDLONP_EMUCFG_STRING, index);

	config->string_entries[index] = value;
	return SDLONP_EMUCFG_SUCCESS;
}


sdlonp_emuconfig_result sdlonp_emuconfig_get_int8(struct sdlonp_emuconfig* config, int index, int8_t* value)
{
	if (index >= SDLONP_EMUCFG_NUM_INT8_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_unchanged(config, SDLONP_EMUCFG_INT8, index);

	*value = config->int8_entries[index];
	return SDLONP_EMUCFG_SUCCESS;
}


sdlonp_emuconfig_result sdlonp_emuconfig_get_int32(struct sdlonp_emuconfig* config, int index, int32_t* value)
{
	if (index >= SDLONP_EMUCFG_NUM_INT32_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_unchanged(config, SDLONP_EMUCFG_INT32, index);

	*value = config->int32_entries[index];
	return SDLONP_EMUCFG_SUCCESS;
}


sdlonp_emuconfig_result sdlonp_emuconfig_get_float(struct sdlonp_emuconfig* config, int index, float* value)
{
	if (index >= SDLONP_EMUCFG_NUM_FLOAT_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_unchanged(config, SDLONP_EMUCFG_FLOAT, index);

	*value = config->float_entries[index];
	return SDLONP_EMUCFG_SUCCESS;
}


/* ================= Public API ================= */

struct sdlonp_emuconfig* sdlonp_emuconfig_new()
{
	const size_t size = sizeof(struct sdlonp_emuconfig);
	struct sdlonp_emuconfig* config = SDL_malloc(size);
	if (config == NULL) {
		SDL_OutOfMemory();
		return NULL;
	}

	SDL_memset(config, 0, size);
	return config;
}


void sdlonp_emuconfig_free(struct sdlonp_emuconfig* config)
{
	/* Free string entries */
	for (int i = 0; i < SDLONP_EMUCFG_NUM_STRING_ENTRIES; ++i) {
		char* entry = config->string_entries[i];
		if (entry != NULL)
			SDL_free(entry);
	}

	SDL_free(config);
}


sdlonp_emuconfig_result sdlonp_emuconfig_parse_value(struct sdlonp_emuconfig* config, char* buffer, int length)
{
	#define SDLONP_ADVANCE_BUFFER_BY(num_bytes)  \
			buffer += (num_bytes);     \
			length -= (num_bytes);

	if (length < 1)
		return SDLONP_EMUCFG_BUFFER_UNDERFLOW;

	const int id = (int) *buffer;
	SDLONP_ADVANCE_BUFFER_BY(1);

	const int index = sdlonp_emuconfig_id_to_index(id);
	const int type = sdlonp_emuconfig_id_to_type(id);

	switch (type)
	{
		case SDLONP_EMUCFG_INT8: {
			if (length < 1)
				return SDLONP_EMUCFG_BUFFER_UNDERFLOW;

			const int8_t value = *((int8_t*) buffer);
			return sdlonp_emuconfig_set_int8(config, index, value);
		}

		case SDLONP_EMUCFG_INT32: {
			if (length < 4)
				return SDLONP_EMUCFG_BUFFER_UNDERFLOW;

			/* Convert bytes from network-order to little-endian order */
			union sdlonp_value value;
			value.u8[0] = buffer[3];
			value.u8[1] = buffer[2];
			value.u8[2] = buffer[1];
			value.u8[3] = buffer[0];
			return sdlonp_emuconfig_set_int32(config, index, value.s32);
		}

		case SDLONP_EMUCFG_FLOAT: {
			if (length < 4)
				return SDLONP_EMUCFG_BUFFER_UNDERFLOW;

			/* Convert bytes from network-order to little-endian order */
			union sdlonp_value value;
			value.u8[0] = buffer[3];
			value.u8[1] = buffer[2];
			value.u8[2] = buffer[1];
			value.u8[3] = buffer[0];
			return sdlonp_emuconfig_set_float(config, index, value.f32);
		}

		case SDLONP_EMUCFG_STRING: {
			/* The passed buffer is expected to point to a 2-byte
			 * length prefix, followed by the actual ASCII data. */
			if (length < 2)
				return SDLONP_EMUCFG_BUFFER_UNDERFLOW;

			/* Convert bytes from network-order to little-endian order */
			union sdlonp_value rawval;
			rawval.u8[0] = buffer[1];
			rawval.u8[1] = buffer[0];
			rawval.u8[2] = 0;
			rawval.u8[3] = 0;

			SDLONP_ADVANCE_BUFFER_BY(2);

			const int value_length = rawval.s32;
			if (length < value_length)
				return SDLONP_EMUCFG_BUFFER_UNDERFLOW;

			/* Copy the string */
			char* value = SDL_malloc(value_length + 1);
			SDL_memcpy(value, buffer, value_length);
			value[value_length] = '\0';

			return sdlonp_emuconfig_set_string(config, index, value);
		}

		default:
			return SDLONP_EMUCFG_INVALID_TYPE;
	}

	#undef SDLONP_ADVANCE_BUFFER_BY
}


int sdlonp_emuconfig_is_valid(struct sdlonp_emuconfig* config, enum sdlonp_emuconfig_id id)
{
	const int type = sdlonp_emuconfig_id_to_type(id) >> SDLONP_EMUCFG_TYPE_SHIFT;
	const uint32_t flag = 1 << sdlonp_emuconfig_id_to_index(id);
	const uint32_t result = config->validmasks[type] & flag;
	return (result != 0) ? 1 : 0;
}


int sdlonp_emuconfig_has_changed(struct sdlonp_emuconfig* config, enum sdlonp_emuconfig_id id)
{
	const int type = sdlonp_emuconfig_id_to_type(id) >> SDLONP_EMUCFG_TYPE_SHIFT;
	const uint32_t flag = 1 << sdlonp_emuconfig_id_to_index(id);
	const uint32_t result = config->dirtymasks[type] & flag;
	return (result != 0) ? 1 : 0;
}


sdlonp_emuconfig_result sdlonp_emuconfig_set_value(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, void* value)
{
	#define	CAST(type, ptr) *((type*) ptr)

	const int index = sdlonp_emuconfig_id_to_index(id);
	const int type = sdlonp_emuconfig_id_to_type(id);

	switch (type)
	{
		case SDLONP_EMUCFG_INT8:
			return sdlonp_emuconfig_set_int8(config, index, CAST(int8_t, value));

		case SDLONP_EMUCFG_INT32:
			return sdlonp_emuconfig_set_int32(config, index, CAST(int32_t, value));

		case SDLONP_EMUCFG_FLOAT:
			return sdlonp_emuconfig_set_float(config, index, CAST(float, value));

		case SDLONP_EMUCFG_STRING:
			return sdlonp_emuconfig_set_string(config, index, (char*) value);

		default:
			return SDLONP_EMUCFG_INVALID_TYPE;
	}

	#undef CAST
}


sdlonp_emuconfig_result sdlonp_emuconfig_get_value(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, void* value)
{
	#define	CAST(type, ptr) (type*) ptr

	if (!sdlonp_emuconfig_is_valid(config, id))
		return SDLONP_EMUCFG_NOT_FOUND;

	const int index = sdlonp_emuconfig_id_to_index(id);
	const int type = sdlonp_emuconfig_id_to_type(id);

	switch (type)
	{
		case SDLONP_EMUCFG_INT8:
			return sdlonp_emuconfig_get_int8(config, index, CAST(int8_t, value));

		case SDLONP_EMUCFG_INT32:
			return sdlonp_emuconfig_get_int32(config, index, CAST(int32_t, value));

		case SDLONP_EMUCFG_FLOAT:
			return sdlonp_emuconfig_get_float(config, index, CAST(float, value));

		default:
			return SDLONP_EMUCFG_INVALID_TYPE;
	}

	#undef CAST
}


sdlonp_emuconfig_result sdlonp_emuconfig_get_string(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, char** value)
{
	if (sdlonp_emuconfig_id_to_type(id) != SDLONP_EMUCFG_STRING)
		return SDLONP_EMUCFG_INVALID_TYPE;

	if (!sdlonp_emuconfig_is_valid(config, id))
		return SDLONP_EMUCFG_NOT_FOUND;

	const int index = sdlonp_emuconfig_id_to_index(id);
	if (index >= SDLONP_EMUCFG_NUM_STRING_ENTRIES)
		return SDLONP_EMUCFG_OUT_OF_RANGE;

	sdlonp_emuconfig_mark_as_unchanged(config, SDLONP_EMUCFG_STRING, index);

	*value = config->string_entries[index];
	return SDLONP_EMUCFG_SUCCESS;
}
