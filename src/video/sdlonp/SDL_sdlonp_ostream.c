#include "SDL_sdlonp_ostream.h"


SDLONP_OutputStream* SDLONP_CreateOutputStream()
{
	SDL_mutex* mutex = SDL_CreateMutex();
	if (mutex == NULL)
		return NULL;

	SDLONP_OutputStream* ostream = SDL_malloc(sizeof(SDLONP_OutputStream));
	if (ostream == NULL)
		goto cleanup_on_error;

	ostream->mutex = mutex;
	ostream->fd = -1;

	return ostream;

	cleanup_on_error: {

		if (mutex != NULL)
			SDL_DestroyMutex(mutex);

		return NULL;
	}
}


void SDLONP_DestroyOutputStream(SDLONP_OutputStream* ostream)
{
	if (ostream->mutex != NULL)
		SDL_DestroyMutex(ostream->mutex);

	SDL_free(ostream);
	ostream = NULL;
}
