#ifndef __SDL_SDLONP_EMUCONFIG_H
#define __SDL_SDLONP_EMUCONFIG_H

#include "SDL_config.h"
#include "SDL_stdinc.h"

/** Return codes of emuconfig's operations */
enum sdlonp_emuconfig_result
{
	SDLONP_EMUCFG_SUCCESS           = 0,
	SDLONP_EMUCFG_ERROR             = -1,
	SDLONP_EMUCFG_INVALID_TYPE      = -2,
	SDLONP_EMUCFG_NOT_FOUND         = -3,
	SDLONP_EMUCFG_OUT_OF_RANGE      = -4,
	SDLONP_EMUCFG_BUFFER_UNDERFLOW  = -5,
};

/** Types of configuration values */
enum sdlonp_emuconfig_type
{
	SDLONP_EMUCFG_INT8    = 0x0 << 6,
	SDLONP_EMUCFG_INT32   = 0x1 << 6,
	SDLONP_EMUCFG_FLOAT   = 0x2 << 6,
	SDLONP_EMUCFG_STRING  = 0x3 << 6,
};

/** IDs of configuration values */
enum sdlonp_emuconfig_id
{
	/* String values */
	SDLONP_EMUCFG_IOSOCKET           = SDLONP_EMUCFG_STRING | 0,
	SDLONP_EMUCFG_CRT_FILTER         = SDLONP_EMUCFG_STRING | 1,
	SDLONP_EMUCFG_CRT_PRESET         = SDLONP_EMUCFG_STRING | 2,
	SDLONP_EMUCFG_KBD_MODEL          = SDLONP_EMUCFG_STRING | 3,
	SDLONP_EMUCFG_KBD_LAYOUT         = SDLONP_EMUCFG_STRING | 4,
	SDLONP_EMUCFG_KBD_CLIENT_MODEL   = SDLONP_EMUCFG_STRING | 5,
	SDLONP_EMUCFG_KBD_CLIENT_LAYOUT  = SDLONP_EMUCFG_STRING | 6,

	/* Int8 values */
	SDLONP_EMUCFG_RELATIVE_MOUSE     = SDLONP_EMUCFG_INT8 | 0,
	SDLONP_EMUCFG_HARD_TERMINATION   = SDLONP_EMUCFG_INT8 | 1,

	/* Int32 values */
	SDLONP_EMUCFG_INACTIVITY_TIMEOUT = SDLONP_EMUCFG_INT32 | 0,
};

typedef enum sdlonp_emuconfig_result sdlonp_emuconfig_result;
typedef enum sdlonp_emuconfig_type sdlonp_emuconfig_type;
typedef enum sdlonp_emuconfig_id sdlonp_emuconfig_id;

/** Emulator's configuration */
struct sdlonp_emuconfig;

/** Creates a new configuration instance. */
struct sdlonp_emuconfig* sdlonp_emuconfig_new();

/** Destroys the specified configuration instance. */
void sdlonp_emuconfig_free(struct sdlonp_emuconfig* config);

/** Parses a new configuration value from specified buffer. */
sdlonp_emuconfig_result sdlonp_emuconfig_parse_value(struct sdlonp_emuconfig* config, char* buffer, int length);

/** Returns 1 if the specified ID represents a valid value, else 0. */
int sdlonp_emuconfig_is_valid(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id);

/** Returns 1 if the specified ID represents an updated value, else 0. */
int sdlonp_emuconfig_has_changed(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id);

/** Sets the configuration value without transfering ownership. */
sdlonp_emuconfig_result sdlonp_emuconfig_set_value(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, void* value);

/** Returns a reference to the specified configuration value. */
sdlonp_emuconfig_result sdlonp_emuconfig_get_value(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, void* value);

/** Returns a reference to the specified configuration string. */
sdlonp_emuconfig_result sdlonp_emuconfig_get_string(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, char** value);

#endif /* __SDL_SDLONP_EMUCONFIG_H */
