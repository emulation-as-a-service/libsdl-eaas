/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#include "SDL_config.h"
#include "SDL.h"
#include "../../events/SDL_events_c.h"
#include "SDL_sdlonp_events.h"
#include "SDL_sdlonp_ostream.h"
#include "SDL_sdlonp_emuconfig.h"
#include "SDL_sdlonp_shared_data.h"
#include "SDL_sdlonp_ipc.h"
#include "sdlonp_keycode_index.h"

#include <guacamole/sdlonp/protocol.h>
#include <guacamole/sdlonp/stream.h>
#include <xkbcommon/xkbcommon.h>
#include <syslog.h>
#include <time.h>


/* Logging Helpers */
#define SDLONP_LOGF(_frmt_, ...) \
	{	struct timespec time;  \
		clock_gettime(CLOCK_MONOTONIC, &time);  \
		printf("%ld.%ld [SDLONP-EVENTS] "_frmt_"\n", time.tv_sec, time.tv_nsec, ##__VA_ARGS__); \
		syslog(LOG_USER | LOG_INFO, "[SDLONP-EVENTS] "_frmt_, ##__VA_ARGS__); }

#ifdef SDLONP_ENABLE_DEBUG_MESSAGES
#	define SDLONP_DEBUGF(_frmt_, ...)  SDLONP_LOGF(_frmt_, __VA_ARGS__)
#else
#	define SDLONP_DEBUGF(_frmt_, ...)  { }
#endif /* SDLONP_ENABLE_DEBUG_MESSAGES */


/* Timeout for mouse-events */
#define SDLONP_MOUSE_EVENT_DELAY  100

/* Default values for keyboard settings */
#define SDLONP_KBMODEL_DEFAULT    "pc105"
#define SDLONP_KBLAYOUT_DEFAULT   "us"
#define SDLONP_KBVARIANT_DEFAULT  NULL
#define SDLONP_KBOPTIONS_DEFAULT  NULL

/* Constants for keysym-mapping */
#define SDLONP_KEYSYM_MASK  0xFF
#define SDLONP_NUM_KEYSYMS  256

/* Mapping for keysyms representing ASCII symbols */
static SDLKey __sdlonp_keysyms[SDLONP_NUM_KEYSYMS];

/* Map an X11 keysym to an SDL keysym */
#define SDLONP_MAP_KEYSYM(x11_keysym, sdl_keysym) \
		__sdlonp_keysyms[(x11_keysym) & SDLONP_KEYSYM_MASK] = (sdl_keysym)

/* Look up an SDL keysym using X11 keysym */
#define SDLONP_LOOKUP_KEYSYM(x11_keysym) \
		__sdlonp_keysyms[(x11_keysym) & SDLONP_KEYSYM_MASK]


/* ==================== Internal Helper-Functions ==================== */

static char* SDLONP_GetStringConfig(struct sdlonp_emuconfig* config, sdlonp_emuconfig_id id, char* default_value)
{
	char* value = NULL;

	if (sdlonp_emuconfig_get_string(config, id, &value) == SDLONP_EMUCFG_SUCCESS)
		return value;

	return default_value;
}


static void SDLONP_PrintRuleNames(char* prefix, const struct xkb_rule_names* names)
{
	SDLONP_LOGF("%s keyboard model: %s", prefix, names->model);
	SDLONP_LOGF("%s keyboard layout: %s", prefix, names->layout);
	if (names->variant != NULL)
		SDLONP_LOGF("%s keyboard variant: %s", prefix, names->variant);

	if (names->options != NULL)
		SDLONP_LOGF("%s keyboard options: %s", prefix, names->options);
}


static int SDLONP_InitEmulatorKeyboard(SDL_PrivateVideoData* hidden, struct sdlonp_emuconfig* emuconfig)
{
	/* RMLVO names for the keymap */
	struct xkb_rule_names names = {
			.rules   = NULL,
			.model   = SDLONP_GetStringConfig(emuconfig, SDLONP_EMUCFG_KBD_MODEL, SDLONP_KBMODEL_DEFAULT),
			.layout  = SDLONP_GetStringConfig(emuconfig, SDLONP_EMUCFG_KBD_LAYOUT, SDLONP_KBLAYOUT_DEFAULT),
			.variant = SDLONP_KBVARIANT_DEFAULT,
			.options = SDLONP_KBOPTIONS_DEFAULT
	};

	SDLONP_PrintRuleNames("Emulator's", &names);

	/* Compile the emulator's keymap */
	struct xkb_keymap* keymap = xkb_keymap_new_from_names(hidden->keyboard_context, &names, XKB_KEYMAP_COMPILE_NO_FLAGS);
	if (keymap == NULL) {
		SDLONP_LOGF("Compilation of emulator's keymap failed!");
		return -1;
	}

	if (xkb_keymap_num_layouts(keymap) != 1) {
		SDLONP_LOGF("Multiple layouts found in emulator's keymap! This is not supported.");
		goto cleanup_on_error;
	}

	hidden->keyboard_state = xkb_state_new(keymap);
	if (hidden->keyboard_state == NULL) {
		SDLONP_LOGF("Initialization of emulator's keyboard state failed!");
		goto cleanup_on_error;
	}

	xkb_keymap_unref(keymap);
	return 0;

	cleanup_on_error: {
		xkb_keymap_unref(keymap);
		return -1;
	}
}


static void SDLONP_CleanupEmulatorKeyboard(SDL_PrivateVideoData* hidden)
{
	if (hidden->keyboard_state != NULL)
		xkb_state_unref(hidden->keyboard_state);
}


static int SDLONP_InitClientKeyboard(SDL_PrivateVideoData* hidden, struct sdlonp_emuconfig* emuconfig)
{
	/* RMLVO names for the keymap */
	struct xkb_rule_names names = {
			.rules   = NULL,
			.model   = SDLONP_GetStringConfig(emuconfig, SDLONP_EMUCFG_KBD_CLIENT_MODEL, SDLONP_KBMODEL_DEFAULT),
			.layout  = SDLONP_GetStringConfig(emuconfig, SDLONP_EMUCFG_KBD_CLIENT_LAYOUT, SDLONP_KBLAYOUT_DEFAULT),
			.variant = SDLONP_KBVARIANT_DEFAULT,
			.options = SDLONP_KBOPTIONS_DEFAULT
	};

	SDLONP_PrintRuleNames("Client's", &names);

	/* Compile the client's keymap */
	struct xkb_keymap* keymap = xkb_keymap_new_from_names(hidden->keyboard_context, &names, XKB_KEYMAP_COMPILE_NO_FLAGS);
	if (keymap == NULL) {
		SDLONP_LOGF("Compilation of client's keymap failed!");
		return -1;
	}

	if (xkb_keymap_num_layouts(keymap) != 1) {
		SDLONP_LOGF("Multiple layouts found in client's keymap! This is not supported.");
		goto cleanup_on_error;
	}

	/* Build an index: keysym -> keycode */
	struct sdlonp_keycode_index* index = sdlonp_keycode_index_new(256);
	if (index == NULL) {
		SDLONP_LOGF("Could not allocate memory for client's keycode index!");
		goto cleanup_on_error;
	}

	sdlonp_keycode_index_build(index, keymap);
	hidden->keycode_index = index;
	xkb_keymap_unref(keymap);

	return 0;

	cleanup_on_error: {
		xkb_keymap_unref(keymap);
		return -1;
	}
}


static void SDLONP_CleanupClientKeyboard(SDL_PrivateVideoData* hidden)
{
	if (hidden->keycode_index != NULL)
		sdlonp_keycode_index_free(hidden->keycode_index);
}


static void SDLONP_InitKesymsMapping()
{
	/* Reset the lookup table */
	for (int i = 0; i < SDLONP_NUM_KEYSYMS; ++i)
		SDLONP_MAP_KEYSYM(i, SDLK_UNKNOWN);

	/* Define all valid constants beween 0xFF00 and 0xFFFF */

	/* TTY function keys */
	SDLONP_MAP_KEYSYM( XKB_KEY_BackSpace    , SDLK_BACKSPACE   );
	SDLONP_MAP_KEYSYM( XKB_KEY_Tab          , SDLK_TAB         );
	SDLONP_MAP_KEYSYM( XKB_KEY_Clear        , SDLK_CLEAR       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Return       , SDLK_RETURN      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Pause        , SDLK_PAUSE       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Escape       , SDLK_ESCAPE      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Delete       , SDLK_DELETE      );

	/* Numeric keypad */
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Decimal   , SDLK_KP_PERIOD   );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Divide    , SDLK_KP_DIVIDE   );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Multiply  , SDLK_KP_MULTIPLY );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Subtract  , SDLK_KP_MINUS    );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Add       , SDLK_KP_PLUS     );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Enter     , SDLK_KP_ENTER    );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_Equal     , SDLK_KP_EQUALS   );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_0         , SDLK_KP0         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_1         , SDLK_KP1         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_2         , SDLK_KP2         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_3         , SDLK_KP3         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_4         , SDLK_KP4         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_5         , SDLK_KP5         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_6         , SDLK_KP6         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_7         , SDLK_KP7         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_8         , SDLK_KP8         );
	SDLONP_MAP_KEYSYM( XKB_KEY_KP_9         , SDLK_KP9         );

	/* Arrows + Home/End pad */
	SDLONP_MAP_KEYSYM( XKB_KEY_Up           , SDLK_UP          );
	SDLONP_MAP_KEYSYM( XKB_KEY_Down         , SDLK_DOWN        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Right        , SDLK_RIGHT       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Left         , SDLK_LEFT        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Insert       , SDLK_INSERT      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Home         , SDLK_HOME        );
	SDLONP_MAP_KEYSYM( XKB_KEY_End          , SDLK_END         );
	SDLONP_MAP_KEYSYM( XKB_KEY_Page_Up      , SDLK_PAGEUP      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Page_Down    , SDLK_PAGEDOWN    );

	/* Function keys */
	SDLONP_MAP_KEYSYM( XKB_KEY_F1           , SDLK_F1          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F2           , SDLK_F2          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F3           , SDLK_F3          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F4           , SDLK_F4          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F5           , SDLK_F5          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F6           , SDLK_F6          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F7           , SDLK_F7          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F8           , SDLK_F8          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F9           , SDLK_F9          );
	SDLONP_MAP_KEYSYM( XKB_KEY_F10          , SDLK_F10         );
	SDLONP_MAP_KEYSYM( XKB_KEY_F11          , SDLK_F11         );
	SDLONP_MAP_KEYSYM( XKB_KEY_F12          , SDLK_F12         );
	SDLONP_MAP_KEYSYM( XKB_KEY_F13          , SDLK_F13         );
	SDLONP_MAP_KEYSYM( XKB_KEY_F14          , SDLK_F14         );
	SDLONP_MAP_KEYSYM( XKB_KEY_F15          , SDLK_F15         );

	/* Key state modifier keys */
	SDLONP_MAP_KEYSYM( XKB_KEY_Num_Lock     , SDLK_NUMLOCK     );
	SDLONP_MAP_KEYSYM( XKB_KEY_Caps_Lock    , SDLK_CAPSLOCK    );
	SDLONP_MAP_KEYSYM( XKB_KEY_Scroll_Lock  , SDLK_SCROLLOCK   );
	SDLONP_MAP_KEYSYM( XKB_KEY_Shift_R      , SDLK_RSHIFT      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Shift_L      , SDLK_LSHIFT      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Control_R    , SDLK_RCTRL       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Control_L    , SDLK_LCTRL       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Alt_R        , SDLK_RALT        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Alt_L        , SDLK_LALT        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Meta_R       , SDLK_RMETA       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Meta_L       , SDLK_LMETA       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Super_L      , SDLK_LSUPER      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Super_R      , SDLK_RSUPER      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Mode_switch  , SDLK_MODE        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Multi_key    , SDLK_COMPOSE     );

	/* Misc. function keys */
	SDLONP_MAP_KEYSYM( XKB_KEY_Help         , SDLK_HELP        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Print        , SDLK_PRINT       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Sys_Req      , SDLK_SYSREQ      );
	SDLONP_MAP_KEYSYM( XKB_KEY_Break        , SDLK_BREAK       );
	SDLONP_MAP_KEYSYM( XKB_KEY_Menu         , SDLK_MENU        );
	SDLONP_MAP_KEYSYM( XKB_KEY_EuroSign     , SDLK_EURO        );
	SDLONP_MAP_KEYSYM( XKB_KEY_Undo         , SDLK_UNDO        );
}


static SDLKey SDLONP_X11KeysymToSDLKey(xkb_keysym_t keysym)
{
	/* Remap X11-KeySyms to SDL-KeySyms */

	/* Fast path for SDLK_SPACE to SDLK_z constants */
	if ((keysym >= XKB_KEY_space) && (keysym <= XKB_KEY_asciitilde))
		return (SDLKey) keysym;

	/* Fast path for SDLK_WORLD_0 to SDLK_WORLD_95 constants */
	if ((keysym >= XKB_KEY_nobreakspace) && (keysym <= XKB_KEY_ydiaeresis))
		return (SDLKey) keysym;

	/* Alternative name for ALTGr key? */
	if (keysym == XKB_KEY_ISO_Level3_Shift)
		return SDLK_RALT;

	/* Not an ASCII symbol? */
	if ((keysym < 0xFF00) || (keysym > 0xFFFF)) {
		SDLONP_LOGF("X11 keysym 0x%X is not defined in SDL!", keysym);
		return SDLK_UNKNOWN;
	}

	/* Mapping for other supported symbols */
	return SDLONP_LOOKUP_KEYSYM(keysym);
}


static void SDLONP_HandleReceivedMessage(_THIS, struct sdlonp_ipc_message* message)
{
	SDLONP_SharedData* sdata = SDLONP_GetSharedData();

	switch (message->type) {

		case SDLONP_IPCMSG_CONFIG_ENTRY: {
			int rc = sdlonp_emuconfig_parse_value(sdata->emuconfig, message->data, message->length);
			if (rc != SDLONP_IPCRES_SUCCESS)
				SDLONP_LOGF("Parsing new emulator's configuration failed! Return code: %i", rc);

			break;
		}

		case SDLONP_IPCMSG_CONFIG_UPDATE: {
			SDLONP_LOGF("Emulator's reconfiguration is currently not implemented!");
			break;
		}

		case SDLONP_IPCMSG_ATTACH_CLIENT: {
			if (SDLONP_AttachClient(this) < 0)
				SDLONP_LOGF("Failed to attach new client!");

			break;
		}

		case SDLONP_IPCMSG_DETACH_CLIENT: {
			if (SDLONP_DetachClient(this, 1) < 0)
				SDLONP_LOGF("Failed to detach client!");

			break;
		}

		case SDLONP_IPCMSG_TERMINATE: {
			SDLONP_LOGF("Emulator shutdown requested.");
			if (this->hidden->hard_termination == 1) {
				SDLONP_LOGF("Forcing termination...");
				exit(0);  /* SDL should still call cleanup callbacks! */
			}
			else {
				SDL_ProcessEvents[SDL_QUIT] = SDL_ENABLE;
				SDL_PrivateQuit();  /* Post quit-event! */
			}
			break;
		}

		case SDLONP_IPCMSG_EMULATOR_COMMAND: {
			/* Forward emulator-specific command as a user event */
			SDL_Event event;
			SDL_memset(&event, 0, sizeof(event));
			event.type = SDL_USEREVENT;
			event.user.type = SDL_USEREVENT;
			event.user.data1 = SDL_malloc(message->length);
			SDL_memcpy(event.user.data1, message->data, message->length);
			event.user.data2 = (void*) message->length;
			SDL_PushEvent(&event);
			break;
		}

		default:
			SDLONP_LOGF("Unknowm message type received: %hhX", message->type);
	}
}


static void SDLONP_CheckClientActivity(_THIS)
{
	SDL_PrivateVideoData* hidden = this->hidden;

	if (hidden->inactivity_timeout < 1)
		return;  /* Inactivity tracking disabled. */

	if (hidden->last_input_timestamp == 0)
		return;  /* No input received yet. */

	const Uint32 duration = SDL_GetTicks() - hidden->last_input_timestamp;
	if (duration < hidden->inactivity_timeout)
		return;  /* Client seems to be active! */

	/* No client activity detected, notify the server */

	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	struct sdlonp_ipc_message* message = &(shared_data->emumsg);
	sdlonp_ipc_compose_notification(message, SDLONP_IPCEID_CLIENT_INACTIVE);
	sdlonp_ipc_sendto(shared_data->emusock, message, &(shared_data->srvsock_address));

	SDLONP_LOGF("Client is inactive for %i ms.", duration);
	hidden->last_input_timestamp = SDL_GetTicks();
}


/* ==================== Public Functions ==================== */

void SDLONP_PumpEvents(_THIS);
void SDLONP_InitOSKeymap(_THIS);


int SDLONP_InitEventHandling(_THIS)
{
	SDLONP_LOGF("Hooking up event handlers for keyboard and mouse...");
	this->PumpEvents = SDLONP_PumpEvents;
	this->InitOSKeymap = SDLONP_InitOSKeymap;

	SDL_PrivateVideoData* hidden = this->hidden;
	hidden->pending_mouse_event = NULL;
	hidden->last_mouse_timestamp = SDL_GetTicks();
	hidden->last_input_timestamp = 0;
	hidden->last_button_state = 0;

	SDLONP_SharedData* shared_data = SDLONP_GetSharedData();
	struct sdlonp_emuconfig* emuconfig = shared_data->emuconfig;

	int8_t relative_mouse;
	int8_t hard_termination;
	int32_t timeout_in_sec;

	/* Inactivity timeout settings */
	sdlonp_emuconfig_id id = SDLONP_EMUCFG_INACTIVITY_TIMEOUT;
	int rc = sdlonp_emuconfig_get_value(emuconfig, id, &timeout_in_sec);
	if (rc != SDLONP_EMUCFG_SUCCESS || timeout_in_sec < 1) {
		SDLONP_LOGF("Inactivity tracking disabled.");
		hidden->inactivity_timeout = 0;
	}
	else {
		SDLONP_LOGF("Inactivity tracking enabled. Timeout: %i seconds.", timeout_in_sec);
		hidden->inactivity_timeout = timeout_in_sec * 1000;  /* in msec */
	}

	/* Hard/gracefully termination */
	id = SDLONP_EMUCFG_HARD_TERMINATION;
	rc = sdlonp_emuconfig_get_value(emuconfig, id, &hard_termination);
	if ((rc == SDLONP_EMUCFG_SUCCESS) && (hard_termination == 1)) {
		SDLONP_LOGF("Using hard termination.");
		hidden->hard_termination = 1;
	}
	else {
		SDLONP_LOGF("Using gracefully termination.");
		hidden->hard_termination = 0;
	}

	/* Relative mouse settings */
	id = SDLONP_EMUCFG_RELATIVE_MOUSE;
	rc = sdlonp_emuconfig_get_value(emuconfig, id, &relative_mouse);
	if ((rc == SDLONP_EMUCFG_SUCCESS) && (relative_mouse == 1)) {
		SDLONP_LOGF("Using relative mouse-coordinates.");
		hidden->relative_mouse = 1;
	}
	else {
		SDLONP_LOGF("Using absolute mouse-coordinates.");
		hidden->relative_mouse = 0;
	}

	SDL_EnableUNICODE(0);

	/* Initialize the xkbcommon library and the keyboard mappings */

	hidden->keyboard_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (hidden->keyboard_context == NULL) {
		SDLONP_LOGF("Initializing the xkbcommon library failed!");
		return -1;
	}

	if (SDLONP_InitEmulatorKeyboard(hidden, emuconfig) < 0)
		goto cleanup_on_error;

	if (SDLONP_InitClientKeyboard(hidden, emuconfig) < 0)
		goto cleanup_on_error;

	SDLONP_InitKesymsMapping();

	return 0;

	cleanup_on_error: {

		SDLONP_CleanupClientKeyboard(hidden);
		SDLONP_CleanupEmulatorKeyboard(hidden);

		if (hidden->keyboard_context != NULL)
			xkb_context_unref(hidden->keyboard_context);

		return -1;
	}
}


void SDLONP_CleanupEventHandling(_THIS)
{
	SDL_PrivateVideoData* hidden = this->hidden;

	SDLONP_CleanupClientKeyboard(hidden);
	SDLONP_CleanupEmulatorKeyboard(hidden);

	if (hidden->keyboard_context != NULL)
		xkb_context_unref(hidden->keyboard_context);
}


/* ==================== SDL Callback Functions ==================== */

void SDLONP_InitOSKeymap(_THIS)
{
	/*
	 * This function is called by SDL during keyboard-subsystem initialization.
	 *
	 * Do nothing here, since we can't report errors back to SDL and quit,
	 * if the initialization of the keyboard mappings failed!
	 */
}


void SDLONP_AddKeyboardEvent(_THIS, int raw_keysym, int pressed)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	hidden->last_input_timestamp = SDL_GetTicks();

	/* Look up keycode using client's keyboard layout */
	const xkb_keysym_t client_keysym = (xkb_keysym_t) raw_keysym;
	const xkb_keycode_t keycode = sdlonp_keycode_index_lookup(hidden->keycode_index, client_keysym);
	if (xkb_keycode_is_legal_x11(keycode) == 0) {
		SDLONP_LOGF("The scancode for keysym 0x%X was not found! Skipping event...", client_keysym);
		return;
	}

	/* Look up keysym on emulator's keyboard */
	const xkb_keysym_t keysym = xkb_state_key_get_one_sym(hidden->keyboard_state, keycode);

#ifdef SDLONP_ENABLE_KEYEVENT_MESSAGES
	if (pressed == 1) {
		const int keysym_name_max_length = 256;
		char client_keysym_name[keysym_name_max_length];
		char keysym_name[keysym_name_max_length];
		xkb_keysym_get_name(client_keysym, client_keysym_name, keysym_name_max_length);
		xkb_keysym_get_name(keysym, keysym_name, keysym_name_max_length);
		SDLONP_LOGF("Key pressed with keycode 0x%X: '%s' -> '%s'", keycode, client_keysym_name, keysym_name);
	}
#endif

	SDL_keysym sdlkey = {
			.sym = SDLONP_X11KeysymToSDLKey(keysym),
			.scancode = (Uint8) (keycode & 0xFF),
			.mod = KMOD_NONE,  /* Will be set by SDL internally! */
			.unicode = 0       /* Ignore this field for now! */
	};

	/* Pass that event to SDL */
	SDL_PrivateKeyboard(pressed, &sdlkey);
}


void SDLONP_AddMouseEvent(_THIS, int x, int y, int button_state)
{
	// Handle a mouse event from the client

	SDL_PrivateVideoData* hidden = this->hidden;
	hidden->last_input_timestamp = SDL_GetTicks();

	/* Relative mouse-movement received? */
	if (hidden->relative_mouse) {
		x -= hidden->client_width;
		y -= hidden->client_height;
	}

	SDL_PrivateMouseMotion(0, hidden->relative_mouse, x, y);

	/* Check new button status */
	const int last_button_state = hidden->last_button_state;
	const int button_state_changes = last_button_state ^ button_state;
	hidden->last_button_state = button_state;
	if (button_state_changes == 0)
		return;  /* Nothing changed! */

#ifdef SDLONP_ENABLE_DEBUG_MESSAGES
	const Uint32 elapsed = SDL_GetTicks() - hidden->last_mouse_timestamp;
	SDLONP_LOGF("Elapsed time between mouse-events: %u", elapsed);
#endif

	/* Update the released and pressed states on the SDL side */
	int button_mask = SDL_BUTTON(SDL_BUTTON_LEFT);
	for (Uint8 button = SDL_BUTTON_LEFT; button <= SDL_BUTTON_WHEELUP; ++button) {
		if ((button_state_changes & button_mask) != 0) {
			/* Button's state changed, send corresponding event */
			const int mask = button_state & button_mask;
			const Uint8 state = (mask != 0) ? SDL_PRESSED : SDL_RELEASED;
			SDL_PrivateMouseButton(state, button, 0, 0);
		}

		button_mask <<= 1;
	}

	hidden->last_mouse_timestamp = hidden->last_input_timestamp;
}


void SDLONP_PumpEvents(_THIS)
{
	SDLONP_DEBUGF("Pumping events...");

	/* Receive any pending events from guacd */

	SDL_PrivateVideoData* hidden = this->hidden;
	sdlonp_istream* emu_input = hidden->input_stream;
	sdlonp_opcode opcode = SDLONP_OPCODE_INVALID;

	/* Try to process the deferred event... */
	if (hidden->pending_mouse_event != NULL) {
		const Uint32 elapsed = SDL_GetTicks() - hidden->last_mouse_timestamp;
		if (elapsed < SDLONP_MOUSE_EVENT_DELAY)
			return;  /* Delay one more time! */

		/* The deferred event can be processed now... */
		sdlonp_mouse_event* event = hidden->pending_mouse_event;
		SDLONP_AddMouseEvent(this, event->xpos, event->ypos, event->buttons);
		hidden->pending_mouse_event = NULL;
	}

	SDLONP_SharedData* sdata = SDLONP_GetSharedData();

	/* Check and handle pending messages... */
	while (sdlonp_ipc_receive(sdata->emusock, &(sdata->emumsg)) == SDLONP_IPCRES_SUCCESS)
		SDLONP_HandleReceivedMessage(this, &(sdata->emumsg));

	/* Is client already disconnected? */
	if (sdlonp_istream_is_enabled(emu_input) == 0)
		return;  /* Yes */

	/* Try to receive new input events... */
	while ((opcode = sdlonp_read_opcode(emu_input)) != SDLONP_OPCODE_INVALID) {

		switch (opcode)
		{
			case SDLONP_OPCODE_MOUSE: {
				sdlonp_mouse_event* event = sdlonp_read_mouse_event(emu_input);
				if ((hidden->last_button_state ^ event->buttons) != 0) {
					/* State of buttons changed. Delay needed? */
					const Uint32 elapsed = SDL_GetTicks() - hidden->last_mouse_timestamp;
					if (elapsed < SDLONP_MOUSE_EVENT_DELAY) {
						/* Yes. Save pending event for deferred processing! */
						hidden->pending_mouse_event = event;
						return;
					}
				}

				SDLONP_AddMouseEvent(this, event->xpos, event->ypos, event->buttons);
				break;
			}

			case SDLONP_OPCODE_KEY: {
				sdlonp_key_event* event = sdlonp_read_key_event(emu_input);
				SDLONP_AddKeyboardEvent(this, event->keysym, event->pressed);
				break;
			}

			case SDLONP_OPCODE_DISCONNECT: {
				SDLONP_LOGF("Client disconnected! Disable output.");
				SDLONP_DetachClient(this, 1);
				return;  /* Stop reading here! */
			}

			default: {
				SDLONP_LOGF("Invalid SDLONP-Opcode received: '%i'", opcode);
				return;
			}
		}
	}

	/* Check user's activity */
	SDLONP_CheckClientActivity(this);
}

/* end of SDL_sdlonp_events.c */
