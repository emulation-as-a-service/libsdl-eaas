#include "sdlonp_keycode_index.h"
#include "SDL_config.h"
#include "SDL.h"

#include <syslog.h>


/* Logging Helpers */
#define SDLONP_LOGF(_frmt_, ...) \
	{ printf("[SDLONP-EVENTS] "_frmt_"\n", ##__VA_ARGS__); \
	  syslog(LOG_USER | LOG_INFO, "[SDLONP-EVENTS] "_frmt_, ##__VA_ARGS__); }

#ifdef SDLONP_ENABLE_DEBUG_MESSAGES
#	define SDLONP_DEBUGF(_frmt_, ...)  SDLONP_LOGF(_frmt_, __VA_ARGS__)
#else
#	define SDLONP_DEBUGF(_frmt_, ...)  { }
#endif /* SDLONP_ENABLE_DEBUG_MESSAGES */


#define SDLONP_KCIDX_INITIAL_ENTRYLIST_CAPACITY  2


struct sdlonp_kcidx_entry
{
	xkb_keysym_t  keysym;
	xkb_keycode_t keycode;
};


struct sdlonp_kcidx_bucket
{
	struct sdlonp_kcidx_entry* entries;
	uint32_t num_entries;
	uint32_t capacity;
};


struct sdlonp_kcidx_keymapiter_data
{
	struct sdlonp_keycode_index* index;
	const xkb_layout_index_t layout;
	const xkb_keysym_t** keysyms;
};


struct sdlonp_keycode_index
{
	struct sdlonp_kcidx_bucket* buckets;
	uint32_t num_buckets;
	uint32_t index_mask;
};


/* ==================== Internal Helper-Functions ==================== */

static inline struct sdlonp_kcidx_bucket* sdlonp_kcidx_get_bucket(struct sdlonp_keycode_index* index, xkb_keysym_t keysym)
{
	/* If d is power of 2:  n % d == n & (d - 1) */
	uint32_t bucket_id = (uint32_t) keysym & index->index_mask;
	return &(index->buckets[bucket_id]);
}


static struct sdlonp_kcidx_entry* sdlonp_kcidx_find_entry(struct sdlonp_kcidx_bucket* bucket, xkb_keysym_t keysym)
{
	for (uint32_t i = 0; i < bucket->num_entries; ++i) {
		struct sdlonp_kcidx_entry* entry = &(bucket->entries[i]);
		if (entry->keysym == keysym)
			return entry;
	}

	return NULL;
}


static int sdlonp_kcidx_add_entry(struct sdlonp_keycode_index* index, xkb_keysym_t keysym, xkb_keycode_t keycode)
{
	struct sdlonp_kcidx_bucket* bucket = sdlonp_kcidx_get_bucket(index, keysym);

	/* Free space available? */
	if (bucket->num_entries >= bucket->capacity) {
		const size_t entry_size = sizeof(struct sdlonp_kcidx_entry);
		if (bucket->entries == NULL) {
			bucket->capacity = SDLONP_KCIDX_INITIAL_ENTRYLIST_CAPACITY;
			bucket->entries = SDL_malloc(bucket->capacity * entry_size);
			if (bucket->entries == NULL)
				return -1;
		}
		else {
			const size_t new_capacity = bucket->capacity * 2;
			struct sdlonp_kcidx_entry* new_entries = SDL_realloc(bucket->entries, new_capacity * entry_size);
			if (new_entries == NULL)
				return -1;

			bucket->capacity = new_capacity;
			bucket->entries = new_entries;
		}
	}

	/* Avoid the creation of possible duplicate entries */
	struct sdlonp_kcidx_entry* entry = sdlonp_kcidx_find_entry(bucket, keysym);
	if (entry == NULL) {
		/* Nothing found for specified keysym, create new entry */
		entry = &(bucket->entries[bucket->num_entries]);
		entry->keysym  = keysym;
		entry->keycode = keycode;
		++(bucket->num_entries);
	}

	return 0;
}


static void sdlonp_kcidx_reset(struct sdlonp_keycode_index* index)
{
	/* Reset the number of entries only! */
	for (uint32_t i = 0; i < index->num_buckets; ++i)
		index->buckets[i].num_entries = 0;
}


static void sdlonp_kcidx_keymapiter(struct xkb_keymap* keymap, xkb_keycode_t keycode, void* data)
{
	const xkb_layout_index_t num_layouts = xkb_keymap_num_layouts_for_key(keymap, keycode);
	if (num_layouts != 1) {
		if (num_layouts > 1)
			SDLONP_LOGF("Currently, only one layout per keymap is supported! Found %u.", num_layouts);

		return;
	}

	const struct sdlonp_kcidx_keymapiter_data* iterdata = (struct sdlonp_kcidx_keymapiter_data*) data;
	const xkb_layout_index_t layout = iterdata->layout;
	const xkb_keysym_t** keysyms = iterdata->keysyms;

	const xkb_level_index_t num_levels = xkb_keymap_num_levels_for_key(keymap, keycode, layout);

#ifdef SDLONP_ENABLE_KEYMAP_DUMPS
	SDLONP_LOGF("Found %i level(s) for keycode 0x%X in layout %i", num_levels, keycode, layout);

	const int keysym_name_max_length = 256;
	char keysym_name[keysym_name_max_length];
#endif

	/* Loop through all shift-levels... */
	for (xkb_level_index_t level = 0; level < num_levels; ++level) {
		const int numsyms = xkb_keymap_key_get_syms_by_level(keymap, keycode, layout, level, keysyms);
		for (int i = 0; i < numsyms; ++i) {
			const xkb_keysym_t keysym = *keysyms[i];
			sdlonp_kcidx_add_entry(iterdata->index, keysym, keycode);

#ifdef SDLONP_ENABLE_KEYMAP_DUMPS
			int keysym_name_length = xkb_keysym_get_name(keysym, keysym_name, keysym_name_max_length);
			SDLONP_LOGF("    Level %u -> %s (0x%X)", level, keysym_name, keysym);
#endif
		}
	}
}


static int sdlonp_kcidx_to_pow2(uint32_t integer)
{
	/* Round up to the next power of 2 using fast algorithm from:
	 * https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */

	--integer;
	integer |= integer >> 1;
	integer |= integer >> 2;
	integer |= integer >> 4;
	integer |= integer >> 8;
	integer |= integer >> 16;
	++integer;

	return integer;
}


/* ==================== Public API ==================== */

struct sdlonp_keycode_index* sdlonp_keycode_index_new(uint32_t num_buckets)
{
	/* The number of buckets must be power of 2 */
	if ((num_buckets & (num_buckets - 1)) != 0)
		num_buckets = sdlonp_kcidx_to_pow2(num_buckets);

	const size_t index_size = sizeof(struct sdlonp_keycode_index);
	const size_t bucket_size = sizeof(struct sdlonp_kcidx_bucket);
	const size_t bucket_list_size = bucket_size * num_buckets;

	struct sdlonp_kcidx_bucket* buckets = SDL_malloc(bucket_list_size);
	if (buckets == NULL)
		return NULL;

	for (int i = 0; i < num_buckets; ++i) {
		struct sdlonp_kcidx_bucket* bucket = &buckets[i];
		bucket->entries = NULL;
		bucket->num_entries = 0;
		bucket->capacity = 0;
	}

	struct sdlonp_keycode_index* index = SDL_malloc(index_size);
	if (index == NULL) {
		SDL_free(buckets);
		return NULL;
	}

	index->buckets = buckets;
	index->num_buckets = num_buckets;
	index->index_mask = num_buckets - 1;
	return index;
}


void sdlonp_keycode_index_free(struct sdlonp_keycode_index* index)
{
	for (int i = 0; i < index->num_buckets; ++i)
		SDL_free(index->buckets[i].entries);

	SDL_free(index->buckets);
	SDL_free(index);
	index = NULL;
}


void sdlonp_keycode_index_build(struct sdlonp_keycode_index* index, struct xkb_keymap* keymap)
{
	sdlonp_kcidx_reset(index);

	struct sdlonp_kcidx_keymapiter_data iterdata = {
			.index = index,
			.layout = 0,
			.keysyms = SDL_malloc(sizeof(xkb_keysym_t*))
	};

	xkb_keymap_key_for_each(keymap, sdlonp_kcidx_keymapiter, &iterdata);
	SDL_free(iterdata.keysyms);  /* Cleanup the allocated pointer */
}


xkb_keycode_t sdlonp_keycode_index_lookup(struct sdlonp_keycode_index* index, xkb_keysym_t keysym)
{
	struct sdlonp_kcidx_bucket* bucket = sdlonp_kcidx_get_bucket(index, keysym);
	struct sdlonp_kcidx_entry* entry = sdlonp_kcidx_find_entry(bucket, keysym);
	if (entry != NULL)
		return entry->keycode;

	return XKB_KEYCODE_INVALID;
}
