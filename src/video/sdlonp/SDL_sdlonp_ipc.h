#ifndef __SDL_SDLONP_IPC_H
#define __SDL_SDLONP_IPC_H

#include "SDL_config.h"
#include "SDL_stdinc.h"
#include <sys/socket.h>
#include <sys/un.h>

/** Return codes of IPC's operations */
enum sdlonp_ipc_result
{
	SDLONP_IPCRES_SUCCESS = 0,
	SDLONP_IPCRES_ERROR   = -1,
	SDLONP_IPCRES_RETRY   = -2,
};

/** Types of messages */
enum sdlonp_ipc_msgtype
{
	SDLONP_IPCMSG_INVALID        = -1,
	SDLONP_IPCMSG_CONFIG_ENTRY   = 1,
	SDLONP_IPCMSG_CONFIG_UPDATE  = 2,
	SDLONP_IPCMSG_NOTIFICATION   = 3,
	SDLONP_IPCMSG_ATTACH_CLIENT  = 4,
	SDLONP_IPCMSG_DETACH_CLIENT  = 5,
	SDLONP_IPCMSG_TERMINATE      = 6,

	/** Emulator specific comamnd */
	SDLONP_IPCMSG_EMULATOR_COMMAND = 127,
};

/** IDs of notifications */
enum sdlonp_ipc_eventid
{
	SDLONP_IPCEID_EMULATOR_CTLSOCK_READY  = 1,
	SDLONP_IPCEID_EMULATOR_READY          = 2,
	SDLONP_IPCEID_CLIENT_ATTACHED         = 3,
	SDLONP_IPCEID_CLIENT_DETACHED         = 4,
	SDLONP_IPCEID_CLIENT_INACTIVE         = 5,
};

typedef enum sdlonp_ipc_result sdlonp_ipc_result;
typedef enum sdlonp_ipc_msgtype sdlonp_ipc_msgtype;
typedef enum sdlonp_ipc_eventid sdlonp_ipc_eventid;


/** Buffer for messages */
struct sdlonp_ipc_buffer
{
	char*  data;
	size_t capacity;
};

/** A message */
struct sdlonp_ipc_message
{
	/* Read-only fields */
	int8_t type;
	char*  data;
	size_t length;
	size_t capacity;

	/* Internal buffer */
	struct sdlonp_ipc_buffer buffer;
};

/** A handle for an IPC socket*/
struct sdlonp_ipc_socket
{
	int fd;
	int flags;
	int max_msgsize;
	char* address;
};


/** Initializes a message. Must be called on newly allocated messages only! */
sdlonp_ipc_result sdlonp_ipc_init_message(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message);

/** Frees internal buffers. The message will be invalid after this call. */
void sdlonp_ipc_release_message(struct sdlonp_ipc_message* message);

/** Compose a notification-message for specified event. */
void sdlonp_ipc_compose_notification(struct sdlonp_ipc_message* message, sdlonp_ipc_eventid eventid);

/** Initializes the passed sockaddr_un struct. */
void sdlonp_ipc_init_address(struct sockaddr_un* address, char* sockpath);

/** Creates a new IPC socket and binds it to the specified name. */
struct sdlonp_ipc_socket* sdlonp_ipc_create(char* sockname, int socktype);

/* Releases resources of the specified IPC socket. */
void sdlonp_ipc_free(struct sdlonp_ipc_socket* socket);

/** Try to receive a new message from specified socket. */
sdlonp_ipc_result sdlonp_ipc_receive(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message);

/** Try to receive a new message, blocking for specified amount of msecs when the data is not available. */
sdlonp_ipc_result sdlonp_ipc_timedreceive(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message, int timeout);

/** Try to send the message to specified destination. */
sdlonp_ipc_result sdlonp_ipc_sendto(struct sdlonp_ipc_socket* socket, struct sdlonp_ipc_message* message, struct sockaddr_un* dstaddr);

/** Enable non-blocking mode for the specified socket. */
void sdlonp_ipc_enable_nonblocking_mode(struct sdlonp_ipc_socket* socket);

/** Disable non-blocking mode for the specified socket. */
void sdlonp_ipc_disable_nonblocking_mode(struct sdlonp_ipc_socket* socket);

#endif /* __SDL_SDLONP_IPC_H */
