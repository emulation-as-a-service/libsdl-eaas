/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
    
    Michael Farrell
    micolous+sdl@gmail.com
*/

#include "SDL_config.h"

#include "SDL.h"
#include "../../events/SDL_sysevents.h"
#include "../../events/SDL_events_c.h"

#include "SDL_vncvideo.h"
#include "SDL_vncevents_c.h"

#include <rfb/rfb.h>
#include <rfb/rfbproto.h>
#include <rfb/keysym.h>

#define VNC_DOUBLE_CLICK_DELAY 100

#define VNC_ENV_RELATIVE_MOUSE "SDL_RELATIVE_MOUSE"


struct VNC_PrivateEventData
{
	SDL_PrivateVideoData* sdl_video_data;
	Uint32 last_timestamp;
	int last_button_mask;
	int relative_mouse;
	int xpos_offset;
	int ypos_offset;
};

static struct VNC_PrivateEventData VNC_data;


void VNC_PumpEvents(_THIS)
{
	// at the moment, libvncserver is operating on another thread, so we don't
	// pump events here
}

void VNC_InitOSKeymap(_THIS)
{
	// keymaps are irrelevant for VNC
}

// Get scancode from sdl keysym. Mapping to ansi keyboard
uint8_t VNC_GetScancodeFromKey(SDLKey sym)
{
  switch(sym)
  {
    case SDLK_BACKSPACE:		return 0x16;
    case SDLK_TAB:			return 0x17;
    case SDLK_CLEAR:			return 0x0;		// ?
    case SDLK_RETURN:			return 0x24;
    case SDLK_PAUSE:			return 0x7f;
    case SDLK_ESCAPE:			return 0x9;
    case SDLK_SPACE:			return 0x41;
    case SDLK_EXCLAIM:			return 0xa;
    case SDLK_QUOTEDBL:		return 0x30;
    case SDLK_HASH:			return 0xc;
    case SDLK_DOLLAR:			return 0xd;
    case SDLK_AMPERSAND:		return 0x10;
    case SDLK_QUOTE:			return 0x30;
    case SDLK_LEFTPAREN:		return 0x12;
    case SDLK_RIGHTPAREN:		return 0x13;
    case SDLK_ASTERISK:		return 0x11;
    case SDLK_PLUS:			return 0x15;
    case SDLK_COMMA:			return 0x3b;
    case SDLK_MINUS:			return 0x14;
    case SDLK_PERIOD:			return 0x3c;		
    case SDLK_SLASH:			return 0x3d;
    
    case SDLK_0:			return 0x13;
    case SDLK_1:			return 0xa;
    case SDLK_2:			return 0xb;
    case SDLK_3:			return 0xc;
    case SDLK_4:			return 0xd;
    case SDLK_5:			return 0xe;
    case SDLK_6:			return 0xf;
    case SDLK_7:			return 0x10;
    case SDLK_8:			return 0x11;
    case SDLK_9:			return 0x12;
    
    case SDLK_COLON:			return 0x2f;
    case SDLK_SEMICOLON:		return 0x2f;
    case SDLK_LESS:			return 0x3b;
    case SDLK_EQUALS:			return 0x15;
    case SDLK_GREATER:			return 0x3c;
    case SDLK_QUESTION:		return 0x3d;
    case SDLK_AT:			return 0xb;
    case SDLK_LEFTBRACKET:		return 0x22;
    case SDLK_BACKSLASH:		return 0x33;
    case SDLK_RIGHTBRACKET:		return 0x23;
    case SDLK_CARET:			return 0xf;
    case SDLK_UNDERSCORE:		return 0x14;
    case SDLK_BACKQUOTE:		return 0x31;
    
    case SDLK_a:			return 0x26;
    case SDLK_b:			return 0x38;
    case SDLK_c:			return 0x36;
    case SDLK_d:			return 0x28;
    case SDLK_e:			return 0x1a;
    case SDLK_f:			return 0x29;
    case SDLK_g:			return 0x2a;
    case SDLK_h:			return 0x2b;
    case SDLK_i:			return 0x1f;
    case SDLK_j:			return 0x2c;
    case SDLK_k:			return 0x2d;
    case SDLK_l:			return 0x2e;
    case SDLK_m:			return 0x3a;
    case SDLK_n:			return 0x39;
    case SDLK_o:			return 0x20;
    case SDLK_p:			return 0x21;
    case SDLK_q:			return 0x18;
    case SDLK_r:			return 0x1b;
    case SDLK_s:			return 0x27;
    case SDLK_t:			return 0x1c;
    case SDLK_u:			return 0x1e;
    case SDLK_v:			return 0x37;
    case SDLK_w:			return 0x19;
    case SDLK_x:			return 0x35;
    case SDLK_y:			return 0x1d;
    case SDLK_z:			return 0x34;
    
    case SDLK_KP0:			return 0x5a;
    case SDLK_KP1:			return 0x57;
    case SDLK_KP2:			return 0x58;
    case SDLK_KP3:			return 0x59;
    case SDLK_KP4:			return 0x53;
    case SDLK_KP5:			return 0x54;
    case SDLK_KP6:			return 0x55;
    case SDLK_KP7:			return 0x4f;
    case SDLK_KP8:			return 0x50;
    case SDLK_KP9:			return 0x51;
    case SDLK_KP_PERIOD:		return 0x5b;
    case SDLK_KP_DIVIDE:		return 0x6a;
    case SDLK_KP_MULTIPLY:		return 0x3f;
    case SDLK_KP_MINUS:		return 0x52;
    case SDLK_KP_PLUS:			return 0x56;
    case SDLK_KP_ENTER:		return 0x68;
    case SDLK_KP_EQUALS:		return 0x0;		// ?
    
    case SDLK_UP:			return 0x6f;
    case SDLK_DOWN:			return 0x74;
    case SDLK_RIGHT:			return 0x72;
    case SDLK_LEFT:			return 0x71;
    
    case SDLK_INSERT:			return 0x76;
    case SDLK_DELETE:			return 0x77;
    case SDLK_HOME:			return 0x6e;
    case SDLK_END:			return 0x73;
    case SDLK_PAGEUP:			return 0x70;
    case SDLK_PAGEDOWN:		return 0x75;
    
    case SDLK_F1:			return 0x43;
    case SDLK_F2:			return 0x44;
    case SDLK_F3:			return 0x45;
    case SDLK_F4:			return 0x46;
    case SDLK_F5:			return 0x47;
    case SDLK_F6:			return 0x48;
    case SDLK_F7:			return 0x49;
    case SDLK_F8:			return 0x4a;
    case SDLK_F9:			return 0x4b;
    case SDLK_F10:			return 0x4c;
    case SDLK_F11:			return 0x5f;
    case SDLK_F12:			return 0x60;
    case SDLK_F13:			return 0x0;		// ?
    case SDLK_F14:			return 0x0;		// ?
    case SDLK_F15:			return 0x0;		// ?
    
    case SDLK_NUMLOCK:			return 0x4d;
    case SDLK_CAPSLOCK:		return 0x42;
    case SDLK_SCROLLOCK:		return 0x4e;
    case SDLK_RSHIFT:			return 0x3e;
    case SDLK_LSHIFT:			return 0x32;
    case SDLK_RCTRL:			return 0x69;
    case SDLK_LCTRL:			return 0x25;
    case SDLK_RALT:			return 0x6c;
    case SDLK_LALT:			return 0x40;
    case SDLK_RMETA:			return 0x87;
    case SDLK_LMETA:			return 0x0;		// ?
    case SDLK_LSUPER:			return 0x85;
    case SDLK_RSUPER:			return 0x86;
    case SDLK_MODE:			return 0x0;		// ?
    case SDLK_COMPOSE:			return 0x0;		// ?
    case SDLK_HELP:			return 0x0;		// ?
    case SDLK_PRINT:			return 0x6b;
    case SDLK_SYSREQ:			return 0x6b;
    case SDLK_BREAK:			return 0x7f;
    case SDLK_MENU:			return 0x0;		// ?
    case SDLK_POWER:			return 0x0;		// ?
    case SDLK_EURO:			return 0x0;		// ?
    case SDLK_UNDO:			return 0x0;		// ?

    case 0x7e:				return 0x31;		// ~
    case 0x25:				return 0xe;		// %
    case 0x7b:				return 0x22;		// {
    case 0x7d:				return 0x23;		// }
    case 0x7c:				return 0x33;		// |
    
    default:				return 0x0;
  }
}

void VNC_KbdAddEvent(rfbBool down, rfbKeySym keySym, struct _rfbClientRec* cl)
{
	// handle a keyboard event from the vnc client.
	SDL_keysym sdlkey;
	memset(&sdlkey, 0, sizeof(SDL_keysym));
	
	switch (keySym) {
		// we need to remap some keycodes because libvncserver and sdl treat them differently
		case XK_Shift_L:     sdlkey.sym = SDLK_LSHIFT;       break;
		case XK_Shift_R:     sdlkey.sym = SDLK_RSHIFT;       break;
		
		case XK_Control_L:   sdlkey.sym = SDLK_LCTRL;        break;
		case XK_Control_R:   sdlkey.sym = SDLK_RCTRL;        break;

		case XK_Meta_L:      sdlkey.sym = SDLK_LMETA;        break;
		case XK_Meta_R:      sdlkey.sym = SDLK_RMETA;        break;

		case XK_Alt_L:       sdlkey.sym = SDLK_LALT;         break;
		case XK_Alt_R:       sdlkey.sym = SDLK_RALT;         break;
		
		case XK_Super_L:     sdlkey.sym = SDLK_LSUPER;       break;
		case XK_Super_R:     sdlkey.sym = SDLK_RSUPER;       break;
		
		case XK_Escape:	      sdlkey.sym = SDLK_ESCAPE;       break;
		case XK_BackSpace:   sdlkey.sym = SDLK_BACKSPACE;    break;
		case XK_Return:      sdlkey.sym = SDLK_RETURN;       break;
		case XK_Sys_Req:     sdlkey.sym = SDLK_SYSREQ;       break;
		case XK_Pause:       sdlkey.sym = SDLK_PAUSE;        break;
		case XK_Tab:         sdlkey.sym = SDLK_TAB;          break;
		case XK_Clear:       sdlkey.sym = SDLK_CLEAR;        break;
		case XK_Menu:        sdlkey.sym = SDLK_MENU;         break;
		case XK_EuroSign:    sdlkey.sym = SDLK_EURO;         break;
		case XK_Undo:        sdlkey.sym = SDLK_UNDO;         break;
		case XK_Print:       sdlkey.sym = SDLK_PRINT;        break;
		case XK_Help:        sdlkey.sym = SDLK_HELP;         break;
		case XK_Break:       sdlkey.sym = SDLK_BREAK;        break;
		case XK_Mode_switch: sdlkey.sym = SDLK_MODE;         break;
		case XK_Multi_key:   sdlkey.sym = SDLK_COMPOSE;      break;
		
		case XK_Insert:      sdlkey.sym = SDLK_INSERT;       break;
		case XK_Delete:      sdlkey.sym = SDLK_DELETE;       break;
		case XK_Home:        sdlkey.sym = SDLK_HOME;         break;
		case XK_End:         sdlkey.sym = SDLK_END;          break;
		case XK_Page_Up:     sdlkey.sym = SDLK_PAGEUP;       break;
		case XK_Page_Down:   sdlkey.sym = SDLK_PAGEDOWN;     break;
		
		case XK_Left:        sdlkey.sym = SDLK_LEFT;         break;
		case XK_Right:       sdlkey.sym = SDLK_RIGHT;        break;
		case XK_Up:          sdlkey.sym = SDLK_UP;           break;
		case XK_Down:        sdlkey.sym = SDLK_DOWN;         break;

		case XK_Num_Lock:    sdlkey.sym = SDLK_NUMLOCK;      break;
		case XK_Caps_Lock:   sdlkey.sym = SDLK_CAPSLOCK;     break;
		case XK_Scroll_Lock: sdlkey.sym = SDLK_SCROLLOCK;    break;
		
		case XK_KP_Insert:		sdlkey.sym = 0x100;		break;
		case XK_KP_End:			sdlkey.sym = 0x101;		break;
		case XK_KP_Down:		sdlkey.sym = 0x102;		break;
		case XK_KP_Page_Down:		sdlkey.sym = 0x103;		break;
		case XK_KP_Left:		sdlkey.sym = 0x104;		break;
		case 0xff9d:			sdlkey.sym = 0x105;		break;
		case XK_KP_Right:		sdlkey.sym = 0x106;		break;
		case XK_KP_Home:		sdlkey.sym = 0x107;		break;
		case XK_KP_Up:			sdlkey.sym = 0x108;		break;
		case XK_KP_Page_Up:		sdlkey.sym = 0x109;		break;
		case XK_KP_Delete:		sdlkey.sym = 0x10a;		break;
		
		// these characters are printable, so we need to translate them as well.
		case XK_KP_Decimal:  sdlkey.sym = SDLK_KP_PERIOD;    sdlkey.unicode = 0x002E; break;
		case XK_KP_Divide:   sdlkey.sym = SDLK_KP_DIVIDE;    sdlkey.unicode = 0x00F7; break;
		case XK_KP_Multiply: sdlkey.sym = SDLK_KP_MULTIPLY;  sdlkey.unicode = 0x00D7; break;
		case XK_KP_Subtract: sdlkey.sym = SDLK_KP_MINUS;     sdlkey.unicode = 0x2212; break;
		case XK_KP_Add:      sdlkey.sym = SDLK_KP_PLUS;      sdlkey.unicode = 0x002B; break;
		case XK_KP_Enter:    sdlkey.sym = SDLK_KP_ENTER;     sdlkey.unicode = 0x000D; break;

		// the majority of keys however are the same
		default:
			sdlkey.sym = keySym;
	}
	// trap dangerous characters that we haven't translated yet
	// because sometimes when these go through, things crash.
	if (keySym > 0xF000 && !sdlkey.sym) {
		printf("VNC: Warning, unknown high keycode %u recieved.\n", keySym);
		return;
	}
	
	// map to a unicode character.  we're pretty much just going to assume all is well.
	// we don't have a keymap, so pretty much everything is one-to-one.
	//
	// in accordance with the behaviour of other SDL modules, we don't report
	// this for keyup events.
	sdlkey.unicode = keySym > 0xF000 || !down ? sdlkey.unicode : keySym;
	
	// handle capital letters
	if (keySym >= XK_A && keySym <= XK_Z) {
		sdlkey.sym = keySym + (XK_a - XK_A);
		sdlkey.mod = KMOD_SHIFT;
	}
	
	// handle numpad
	if (keySym >= XK_KP_0 && keySym <= XK_KP_9) {
		sdlkey.sym = keySym - (XK_KP_0 - SDLK_KP0);
		sdlkey.unicode = 0x0030 + (keySym - XK_KP_0);
	}
	
	// handle f-keys
	if (keySym >= XK_F1 && keySym <= XK_F15)
		sdlkey.sym = keySym - (XK_F1 - SDLK_F1);
	
	// set scancode
	sdlkey.scancode = VNC_GetScancodeFromKey(sdlkey.sym);

	// debug
	SDL_EnableUNICODE( 1 );
	char uni = '0';
	if ( (sdlkey.unicode & 0xFF80) == 0 ) {
	  uni = sdlkey.unicode & 0x7F;
	}
	printf("keysym.scancode: %#x\n", sdlkey.scancode);
	printf("keysym.sym: %#x\n", sdlkey.sym);
	printf("keysym.mod: %#x\n", sdlkey.mod);
	printf("keysym.unicode: %c\n", uni);
	printf("\n");

	// pump that through to SDL
	SDL_PrivateKeyboard(down, &sdlkey);

}

void VNC_PtrAddEvent (int button_mask, int x, int y, struct _rfbClientRec* cl)
{
	// Handle a mouse event from the client

	SDL_PrivateVideoData* sdl_video = VNC_data.sdl_video_data;
	const int last_button_mask = VNC_data.last_button_mask;

	/* Relative mouse-movement received? */
	if (VNC_data.relative_mouse == TRUE) {
		x -= VNC_data.xpos_offset;
		y -= VNC_data.ypos_offset;
	}

	// Scale the mouse position, when using the NTSC filters
	if (sdl_video->ntsc_filter != NULL) {
		x = (int) ((float) x / sdl_video->width_scale);
		y = (int) ((float) y / sdl_video->height_scale);
	}

	/* All mouse movements are absolute due to the nature of VNC
	 * don't tell SDL the new event just yet. */
	SDL_PrivateMouseMotion(last_button_mask, VNC_data.relative_mouse, x, y);

	VNC_data.last_button_mask = button_mask;
	
	/* Check new button status */
	const int state_changes = last_button_mask ^ button_mask;
	if (state_changes == 0)
		return;  /* Nothing changed! */

	/* State changed, delay the processing slightly */
	const Uint32 timestamp = SDL_GetTicks();
	const Uint32 elapsed = timestamp - VNC_data.last_timestamp;
	if (elapsed < VNC_DOUBLE_CLICK_DELAY)
		SDL_Delay(VNC_DOUBLE_CLICK_DELAY - elapsed);

	int button;

	/* Update the released and pressed states on the SDL side */
	const int released_buttons = state_changes & last_button_mask;
	const int pressed_buttons = state_changes & button_mask;
	const int end = (released_buttons > pressed_buttons) ? released_buttons : pressed_buttons;
	for (button = 1; button <= end; button <<= 1) {
		if (pressed_buttons & button)
			SDL_PrivateMouseButton(SDL_PRESSED, button, 0, 0);
		else if (released_buttons & button)
			SDL_PrivateMouseButton(SDL_RELEASED, button, 0, 0);
	}

	VNC_data.last_timestamp = SDL_GetTicks();
}


void VNC_InitEvents(_THIS)
{
	printf("VNC: Hooking up event handlers for VNC\n");
	rfbScreenInfoPtr vncscr = this->hidden->vncs;
	
	vncscr->kbdAddEvent = VNC_KbdAddEvent;
	vncscr->ptrAddEvent = VNC_PtrAddEvent;

	VNC_data.sdl_video_data = this->hidden;
	VNC_data.last_timestamp = SDL_GetTicks();
	VNC_data.last_button_mask = 0;

	/* Relative mouse settings */
	const char* env = SDL_getenv(VNC_ENV_RELATIVE_MOUSE);
	VNC_data.relative_mouse = (env != NULL);
	if (VNC_data.relative_mouse == TRUE) {
		VNC_data.xpos_offset = vncscr->width;
		VNC_data.ypos_offset = vncscr->height;
	}
	else {
		VNC_data.xpos_offset = 0;
		VNC_data.ypos_offset = 0;
	}
}

/* end of SDL_vncevents.c ... */
