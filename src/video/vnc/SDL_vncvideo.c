/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
        
    Michael Farrell
    micolous+sdl@gmail.com
*/
#include "SDL_config.h"

/* VNC server SDL driver.
 *
 * This acts as a simple VNC server using libvncserver, to allow you to use
 * an SDL application directly over VNC (ie: without an X-server).
 *
 * This code was written by Michael Farrell <micolous+sdl@gmail.com>
 */

#include "SDL_video.h"
#include "SDL_mouse.h"
#include "../SDL_sysvideo.h"
#include "../SDL_pixels_c.h"
#include "../../events/SDL_events_c.h"

#include "SDL_vncvideo.h"
#include "SDL_vncevents_c.h"
#include "SDL_vncmouse_c.h"


#define VNCVID_DRIVER_NAME  "vnc"

/* Environment variables */
#define VNCVID_ENV_FILTER_NAME    "SDL_NTSCFILTER"
#define VNCVID_ENV_FILTER_PRESET  "SDL_NTSCPRESET"

/* NTSC-Video Filters */
#define VNCVID_FILTER_SNES_NTSC  "snes-ntsc"

/* NTSC-Filter Presets */
#define VNCVID_FILTER_PRESET_MONOCHROME  "monochrome"
#define VNCVID_FILTER_PRESET_COMPOSITE   "composite"
#define VNCVID_FILTER_PRESET_SVIDEO      "svideo"
#define VNCVID_FILTER_PRESET_RGB         "rgb"

/* Logging Halpers */
#define VNC_LOGF(_frmt_, ...) \
	printf("[VNC-VIDEO] "_frmt_, ##__VA_ARGS__)

/* Callback for updating screen-rectangles */
typedef void (*UpdateRectsFunc)(_THIS, int numrects, SDL_Rect* rects);


/* Initialization/Query functions */
static int VNC_VideoInit(_THIS, SDL_PixelFormat *vformat);
static SDL_Rect **VNC_ListModes(_THIS, SDL_PixelFormat *format, Uint32 flags);
static SDL_Surface *VNC_SetVideoMode(_THIS, SDL_Surface *current, int width, int height, int bpp, Uint32 flags);
static void VNC_VideoQuit(_THIS);

/* Hardware surface functions */
static int VNC_AllocHWSurface(_THIS, SDL_Surface *surface);
static int VNC_LockHWSurface(_THIS, SDL_Surface *surface);
static void VNC_UnlockHWSurface(_THIS, SDL_Surface *surface);
static void VNC_FreeHWSurface(_THIS, SDL_Surface *surface);

/* etc. */
static void VNC_OnFrameUpdateStart(struct _rfbClientRec* client);
static void VNC_OnFrameUpdateFinish(struct _rfbClientRec* client, int result);
static void VNC_UpdateRects(_THIS, int numrects, SDL_Rect* rectangles);
static void VNC_UpdateRectsSNES(_THIS, int numrects, SDL_Rect* rectangles);
static void VNC_SetCaption(_THIS, const char* title, const char* icon);
static int VNC_SetColors(_THIS, int firstcolor, int ncolors, SDL_Color *colors);


/* VNC driver bootstrap functions */

int VNC_getintenv(const char* name, int default_value) {
	const char *envv = SDL_getenv(name);
	int value = default_value;
	if (envv)
		value = atoi(envv);
		
	return value;
}

static UpdateRectsFunc VNC_GetUpdateRectsCallback()
{
	const char* filter = SDL_getenv(VNCVID_ENV_FILTER_NAME);
	if (filter == NULL) {
		VNC_LOGF("No NTSC-Filter set, use unmodified video-output.\n");
		return VNC_UpdateRects;
	}

	if (SDL_strcmp(filter, VNCVID_FILTER_SNES_NTSC) == 0) {
		VNC_LOGF("Use '%s' filter for video-output.\n", filter);
		return VNC_UpdateRectsSNES;
	}

	VNC_LOGF("Unknown filter '%s' set for video-output.\n", filter);
	return NULL;
}

static snes_ntsc_t* VNC_GetFilterSNES()
{
	/* Default preset */
	snes_ntsc_setup_t setup;

	char* preset = SDL_getenv(VNCVID_ENV_FILTER_PRESET);
	if ((preset == NULL) || (SDL_strcmp(preset, VNCVID_FILTER_PRESET_COMPOSITE) == 0)) {
		setup = snes_ntsc_composite;
		preset = VNCVID_FILTER_PRESET_COMPOSITE;
	}
	else if (SDL_strcmp(preset, VNCVID_FILTER_PRESET_MONOCHROME) == 0)
		setup = snes_ntsc_monochrome;
	else if (SDL_strcmp(preset, VNCVID_FILTER_PRESET_SVIDEO) == 0)
			setup = snes_ntsc_svideo;
	else if (SDL_strcmp(preset, VNCVID_FILTER_PRESET_RGB) == 0)
			setup = snes_ntsc_rgb;
	else {
		VNC_LOGF("Invalid filter-preset '%s' specified.\n", preset);
		return NULL;
	}

	/* Initialize the filter */
	snes_ntsc_t* filter = (snes_ntsc_t*) SDL_malloc(sizeof(snes_ntsc_t));
	if (filter == NULL) {
		VNC_LOGF("Could not allocate memory for snes-ntsc filter.\n");
		return NULL;
	}

	VNC_LOGF("Use filter-preset '%s'.\n", preset);
	snes_ntsc_init(filter, &setup);
	return filter;
}

static void VNC_DoubleFramebufferHeight(int width, int height, int output_pitch, unsigned char* pixels)
{
	/* This code is based on the example from the snes-ntsc library. */

	int y, n;

	for (y = height / 2; --y >= 0; ) {
		unsigned char const* input = pixels + y * output_pitch;
		unsigned char* output = pixels + y * 2 * output_pitch;
		for (n = width; n; --n) {
			unsigned prev = *(unsigned short*) input;
			unsigned next = *(unsigned short*) (input + output_pitch);

			/* mix 16-bit rgb without losing low bits */
			unsigned mixed = prev + next + ((prev ^ next) & 0x0821);

			/* darken by 12% */
			*(unsigned short*) output = prev;
			*(unsigned short*) (output + output_pitch) = (mixed >> 1) - (mixed >> 4 & 0x18E3);

			input += 2;
			output += 2;
		}
	}
}


static int VNC_Available(void)
{
	const char *envr = SDL_getenv("SDL_VIDEODRIVER");
	if ((envr) && (SDL_strcmp(envr, VNCVID_DRIVER_NAME) == 0)) {
		return(1);
	}

	return(0);
}

static void VNC_DeleteDevice(SDL_VideoDevice* device)
{
	SDL_PrivateVideoData* hidden = device->hidden;

	if (hidden->vncscr_mutex != NULL)
		SDL_DestroyMutex(hidden->vncscr_mutex);

	SDL_free(hidden->ntsc_filter);
	SDL_free(hidden);
	SDL_free(device);
}

static SDL_VideoDevice* VNC_CreateDevice(int devindex)
{
	/* Check, whether the callback for updating the screen was specified */
	UpdateRectsFunc update_rect_callback = VNC_GetUpdateRectsCallback();
	if (update_rect_callback == NULL)
		return NULL;

	/* Setup the filter, when filtering was enabled. */
	snes_ntsc_t* filter = NULL;
	if (update_rect_callback != VNC_UpdateRects) {
		filter = VNC_GetFilterSNES();
		if (filter == NULL)
			return NULL;  /* Something gone wrong */
	}

	/* Create the video-device */
	SDL_VideoDevice* device = (SDL_VideoDevice*) SDL_malloc(sizeof(SDL_VideoDevice));
	if (device == NULL)
		goto out_of_memory;

	SDL_memset(device, 0, sizeof(SDL_VideoDevice));

	/* Create the internal video-data */
	SDL_PrivateVideoData* hidden = (SDL_PrivateVideoData*) SDL_malloc(sizeof(SDL_PrivateVideoData));
	if (hidden == NULL)
		goto out_of_memory;

	SDL_memset(hidden, 0, sizeof(SDL_PrivateVideoData));
	device->hidden = hidden;

	/* Create the mutex for VNC synchronization, when filtering enabled */
	if (update_rect_callback != VNC_UpdateRects) {
		hidden->vncscr_mutex = SDL_CreateMutex();
		if (hidden->vncscr_mutex == NULL)
			goto out_of_memory;
	}

	/* Set the specified filter */
	device->UpdateRects = update_rect_callback;
	device->hidden->ntsc_filter = filter;

	/* Set the function pointers */
	device->VideoInit = VNC_VideoInit;
	device->ListModes = VNC_ListModes;
	device->SetVideoMode = VNC_SetVideoMode;
	device->CreateYUVOverlay = NULL;
	device->SetColors = VNC_SetColors;
	device->VideoQuit = VNC_VideoQuit;
	device->AllocHWSurface = VNC_AllocHWSurface;
	device->CheckHWBlit = NULL;
	device->FillHWRect = NULL;
	device->SetHWColorKey = NULL;
	device->SetHWAlpha = NULL;
	device->LockHWSurface = VNC_LockHWSurface;
	device->UnlockHWSurface = VNC_UnlockHWSurface;
	device->FlipHWSurface = NULL;
	device->FreeHWSurface = VNC_FreeHWSurface;
	device->SetCaption = VNC_SetCaption;
	device->SetIcon = NULL;
	device->IconifyWindow = NULL;
	device->GrabInput = NULL;
	device->GetWMInfo = NULL;
	device->InitOSKeymap = VNC_InitOSKeymap;
	device->PumpEvents = VNC_PumpEvents;
	device->free = VNC_DeleteDevice;
	
	// we don't implement relative movement or mouse restraints at all - 
	// everything is absolute this is due to the nature of VNC

	device->FreeWMCursor = VNC_FreeWMCursor;
	device->CreateWMCursor = VNC_CreateWMCursor;
	device->ShowWMCursor = VNC_ShowWMCursor;
	
	device->hidden->viewOnly = VNC_getintenv("SDL_VNC_VIEW_ONLY", 0) == 1;
	return device;

	/* Cleanup */
	out_of_memory: {

		if (filter != NULL)
			SDL_free(filter);

		if (hidden != NULL) {
			if (hidden->vncscr_mutex != NULL)
				SDL_DestroyMutex(hidden->vncscr_mutex);

			SDL_free(hidden);
		}

		if (device != NULL)
			SDL_free(device);

		SDL_OutOfMemory();
		return NULL;
	}
}

VideoBootStrap VNC_bootstrap = {
	VNCVID_DRIVER_NAME, "SDL libvncserver video-driver (with ntsc-filter support)",
	VNC_Available, VNC_CreateDevice
};


int VNC_VideoInit(_THIS, SDL_PixelFormat *vformat)
{
	/*
	fprintf(stderr, "WARNING: You are using the SDL dummy video driver!\n");
	*/
	/* we change this during the SDL_SetVideoMode implementation... */
	vformat->BitsPerPixel = 16;
	vformat->BytesPerPixel = 2;
	
	// make a blank cursor
	this->hidden->hiddenCursor = rfbMakeXCursor(0, 0, "\0", "\0");
	this->hidden->hiddenCursor->cleanup = 0;
	
	/* We're done! */
	return(0);
}

SDL_Rect **VNC_ListModes(_THIS, SDL_PixelFormat *format, Uint32 flags)
{
	// SDL_VNC_DEPTH is useful for pygame applications which try to automatically
	// run at the lowest display depth they can.
	//
	// Unfortunately, they have issues when running with a colour palette.  This
	// hack tricks pygame into using a higher display depth.  Normally, pygame
	// will try depth in the order of: 8, 8, 16, 15, 32.
	//
	// This will force SDL lie about what display depth it supports.  This 
	// defaults to 16 bpp.  If set to 0, it will say it supports all depths.
	//
	// This doesn't stop a program from just calling SDL_SetVideoMode directly.
	int depth = VNC_getintenv("SDL_VNC_DEPTH", 16);
	
	//printf("VNC_ListModes called, asking about SDL_PixelFormat bpp=%i\n", format->BitsPerPixel);
	return (depth == 0 || format->BitsPerPixel == depth) ? (SDL_Rect **) -1 : NULL;
}

/* 
 * used in rfbGetScreen and rfbNewFramebuffer: and estimate to the number
 * of bits per color, of course for some visuals, e.g. 565, the number
 * is not the same for each color.  This is just a sane default.
 */
int guess_bits_per_color(int bits_per_pixel) {
	int bits_per_color;
	
	/* first guess, spread them "evenly" over R, G, and B */
	bits_per_color = bits_per_pixel/3;
	if (bits_per_color < 1) {
		bits_per_color = 1;	/* 1bpp, 2bpp... */
	}

	/* choose safe values for usual cases: */
	if (bits_per_pixel == 8) {
		bits_per_color = 2;
	} else if (bits_per_pixel == 15 || bits_per_pixel == 16) {
		bits_per_color = 5;
	} else if (bits_per_pixel == 24 || bits_per_pixel == 32) {
		bits_per_color = 8;
	}
	return bits_per_color;
}

void rfbUpdatePixelFmt(rfbScreenInfoPtr screen);

SDL_Surface* VNC_SetVideoMode(_THIS, SDL_Surface* current, int width, int height, int bpp, Uint32 flags)
{
	VNC_LOGF("Setting SDL mode to %dx%d, with %i bits-per-pixel.\n", width, height, bpp);

	SDL_PrivateVideoData* hidden = this->hidden;
	rfbScreenInfoPtr vncscr = hidden->vncs;
	const rfbBool first_start = (vncscr == NULL);
	const int bytes_per_pixel = bpp / 8;
	const int bits_per_sample = guess_bits_per_color(bpp);

	/* Free old frame-buffer in SDL */
	if (current->pixels != NULL)
		SDL_free(current->pixels);

	/* Free old frame-buffer in VNC */
	if ((vncscr != NULL) && (vncscr->frameBuffer != NULL)) {
		/* Shared buffers? */
		if (current->pixels != vncscr->frameBuffer)
			SDL_free(vncscr->frameBuffer);  /* No */

		vncscr->frameBuffer = NULL;
	}

	current->pixels = NULL;

	/* Set up the SDL framebuffer */
	current->w = width;
	current->h = height;
	current->flags = flags & SDL_FULLSCREEN;
	current->pitch = current->w * bytes_per_pixel;

	/* Update settings, when NTSC filter was enabled */
	if (this->UpdateRects == VNC_UpdateRectsSNES) {
		if (bpp != 16) {
			const char* message = "Video-output filter '%s' expects 16 bpp, but set was %i bpp.";
			SDL_SetError(message, VNCVID_FILTER_SNES_NTSC, bpp);
			return NULL;
		}

		/* Allocate new one for SDL */
		const size_t framebuf_size = width * height * bytes_per_pixel;
		current->pixels = SDL_malloc(framebuf_size);
		if (current->pixels == NULL) {
			SDL_SetError("Couldn't allocate buffer for SDL framebuffer.");
			return NULL;
		}

		/* Clear the SDL framebuffer */
		SDL_memset(current->pixels, 0, framebuf_size);

		/* Double the size for VNC */
		width = SNES_NTSC_OUT_WIDTH(width);
		height = height * 2;

		hidden->burst_phase = 0;
	}

	/* Update the format structure with defaults for specified bpp */
	SDL_PixelFormat* format = SDL_ReallocFormat(current, bpp, 0,0,0,0);
	if (format == NULL) {
		SDL_SetError("Couldn't allocate new pixel format for requested mode");
		goto cleanup_on_error;
	}

	/* Update the scaling factors for VNC screen */
	hidden->width_scale = (float) width / (float) current->w;
	hidden->height_scale = (float) height / (float) current->h;

	/* Allocate new frame-buffer */
	const size_t framebuf_size = width * height * bytes_per_pixel;
	void* framebuf = SDL_malloc(framebuf_size);
	if (framebuf == NULL) {
		SDL_SetError("Couldn't allocate buffer for VNC framebuffer.");
		goto cleanup_on_error;
	}

	/* Clear the framebuffer */
	SDL_memset(framebuf, 0, framebuf_size);

	/* Do SDL and VNC share the framebuffer? */
	if (this->UpdateRects == VNC_UpdateRects)
		current->pixels = framebuf;  /* Yes, since filtering is disabled! */

	VNC_LOGF("Setting VNC mode to %dx%d, with %i bits-per-pixel.\n", width, height, bpp);

	/* Initialize remote screen */
	if (first_start) {
		vncscr = rfbGetScreen(NULL, NULL, width, height, bits_per_sample, bpp / bits_per_sample, bytes_per_pixel) ;
		vncscr->frameBuffer = framebuf;
		this->hidden->vncs = vncscr;
	}
	else {
		// destroy the colour palette if it exists
		// FIXME uncommenting this code causes things to crash
		// libvncserver likes going around freeing memory on it's own.
		//if (this->hidden->vncs->colourMap.data.bytes)
		//	SDL_free(this->hidden->vncs->colourMap.data.bytes);
	
		rfbNewFramebuffer(vncscr, framebuf, width, height, bits_per_sample, bpp / bits_per_sample, bytes_per_pixel);
	}
	
	if (bpp > 8) {
		rfbPixelFormat* rfbformat = &(vncscr->serverFormat);
		SDL_PixelFormat* sdlformat = current->format;

		rfbformat->trueColour = TRUE;
		rfbformat->redShift   = sdlformat->Rshift;
		rfbformat->greenShift = sdlformat->Gshift;
		rfbformat->blueShift  = sdlformat->Bshift;
		rfbformat->redMax     = sdlformat->Rmask >> sdlformat->Rshift;
		rfbformat->greenMax   = sdlformat->Gmask >> sdlformat->Gshift;
		rfbformat->blueMax    = sdlformat->Bmask >> sdlformat->Bshift;
	}
	else {
		/* It's a colour palette */
		vncscr->serverFormat.trueColour = FALSE;
		
		/* Allocate buffer for palette */
		const uint32_t count = 1 << bpp;
		const size_t length = count * 3;
		uint8_t* bytes = SDL_malloc(length);
		if (bytes == NULL) {
			SDL_OutOfMemory();
			goto cleanup_on_error;
		}
		
		SDL_memset(bytes, 0, length);

		rfbColourMap* colmap = &(vncscr->colourMap);
		colmap->count = count;
		colmap->is16 = FALSE;
		colmap->data.bytes = bytes;
	}
	
	if (first_start) {
		/* Setup VNC-Server */
		int shared = VNC_getintenv("SDL_VNC_ALWAYS_SHARED", 1);
		if (shared != 0 && shared != 1) {
			SDL_SetError("SDL_VNC_ALWAYS_SHARED must be set to either 0 or 1");
			goto cleanup_on_error;
		}
		
		vncscr->alwaysShared = shared == 1;
		int display = VNC_getintenv("SDL_VNC_DISPLAY", -1);
		if (display < -1) {
			SDL_SetError("SDL_VNC_DISPLAY must be > -1");
			goto cleanup_on_error;
		}
		
		if (display > -1) {
			vncscr->autoPort = FALSE;
			vncscr->port = SERVER_PORT_OFFSET + display;
		} else {
			vncscr->autoPort = TRUE;
		}
		
		/* Setup callbacks for SDL and VNC synchronization */
		if (this->UpdateRects != VNC_UpdateRects) {
			vncscr->screenData = hidden->vncscr_mutex;
			vncscr->displayHook = VNC_OnFrameUpdateStart;
			vncscr->displayFinishedHook = VNC_OnFrameUpdateFinish;
		}

		/* Start VNC-Server */
		rfbSetCursor(vncscr, this->hidden->hiddenCursor);
		rfbInitServer(vncscr);
		if (!this->hidden->viewOnly)
			VNC_InitEvents(this);
		rfbRunEventLoop(vncscr, 1000, TRUE);

	} else {
		rfbUpdatePixelFmt(vncscr);
		// TODO make libvncclient push out the updated colourmap to clients.
	}

	/* We're done */
	return current;

	/* Something gone wrong */
	cleanup_on_error: {

		/* Free SDL frame-buffer */
		if (current->pixels != NULL)
			SDL_free(current->pixels);

		/* Free VNC frame-buffer, but only when unshared */
		if ((vncscr != NULL) && (vncscr->frameBuffer != NULL)) {
			if (vncscr->frameBuffer != current->pixels)
				SDL_free(vncscr->frameBuffer);

			vncscr->frameBuffer = NULL;
		}

		current->pixels = NULL;
		return NULL;
	}
}

/* We don't actually allow hardware surfaces other than the main one */
static int VNC_AllocHWSurface(_THIS, SDL_Surface *surface)
{
	return(-1);
}

static void VNC_FreeHWSurface(_THIS, SDL_Surface *surface)
{
	return;
}

/* We need to wait for vertical retrace on page flipped displays */
static int VNC_LockHWSurface(_THIS, SDL_Surface *surface)
{
	return(0);
}

static void VNC_UnlockHWSurface(_THIS, SDL_Surface *surface)
{
	return;
}

static void VNC_OnFrameUpdateStart(struct _rfbClientRec* client)
{
	/* Lock the VNC's framebuffer */
	SDL_mutex* mutex = (SDL_mutex*) client->screen->screenData;
	if (mutex != NULL)
		SDL_LockMutex(mutex);
}

static void VNC_OnFrameUpdateFinish(struct _rfbClientRec* client, int result)
{
	/* Unlock the VNC's framebuffer */
	SDL_mutex* mutex = (SDL_mutex*) client->screen->screenData;
	if (mutex != NULL)
		SDL_UnlockMutex(mutex);
}

static void VNC_UpdateRects(_THIS, int numrects, SDL_Rect* rectangles)
{
	rfbScreenInfoPtr screen = this->hidden->vncs;
	if (screen == NULL)
		return;

	int i;

	for (i = 0; i < numrects; ++i) {
		SDL_Rect* r = &rectangles[i];
		rfbMarkRectAsModified(screen, r->x, r->y, r->x + r->w, r->y + r->h);
	}
}

static void VNC_UpdateRectsSNES(_THIS, int numrects, SDL_Rect* rectangles)
{
	SDL_PrivateVideoData* hidden = this->hidden;
	SDL_Surface* sdlscr = this->screen;
	rfbScreenInfoPtr vncscr = hidden->vncs;
	if (vncscr == NULL)
		return;

	const float wscale = hidden->width_scale;
	const int output_pitch = vncscr->width << 1;

	SDL_LockMutex(hidden->vncscr_mutex);  /* Lock the VNC's framebuffer */

	/* Perform NTSC filtering */
	snes_ntsc_blit(hidden->ntsc_filter, sdlscr->pixels, sdlscr->w, hidden->burst_phase,
					sdlscr->w, sdlscr->h, vncscr->frameBuffer, output_pitch);

	/* Scale the filtered framebuffer 2x vertically */
	VNC_DoubleFramebufferHeight(vncscr->width, vncscr->height, output_pitch, vncscr->frameBuffer);

	SDL_UnlockMutex(hidden->vncscr_mutex);  /* Release the VNC's framebuffer */

	/* Prepare for next run */
	hidden->burst_phase ^= 1;

	int i;

	/* Send updated screen regions to client */
	for (i = 0; i < numrects; ++i) {
		SDL_Rect* rect = &rectangles[i];
		const Sint16 x1 = (Sint16) ((float) rect->x * wscale);
		const Uint16 w  = (Uint16) ((float) rect->w * wscale);
		const Sint16 y1 = rect->y << 1;
		const Sint16 y2 = y1 + (rect->h << 1);

		/* For each last input-pixel, three output-pixels are added */
		Sint16 x2 = x1 + w + 3;
		if (x2 >= vncscr->width)
			x2 = vncscr->width - 1;

		rfbMarkRectAsModified(vncscr, x1, y1, x2, y2);
	}
}


static void VNC_SetCaption(_THIS, const char* title, const char* icon)
{
	// TODO implement caption changing
}


int VNC_SetColors(_THIS, int firstcolour, int ncolours, SDL_Color *colours)
{
	if (this->hidden->vncs->serverFormat.trueColour) {
		// we don't have a colour palette
		return 0;
	} else {
		int x;
		
		printf("updating colours %i - %i (in %i palette)\n", firstcolour, firstcolour+ncolours, this->hidden->vncs->colourMap.count);
		if (this->hidden->vncs->colourMap.count < firstcolour+ncolours) {
			// too many colours sent!
			return 0;
		}
		
		int i=0;
		// colour components are 8-bit
		for (x=firstcolour; x<firstcolour+ncolours; x++) {
			this->hidden->vncs->colourMap.data.bytes[i++] = colours[x].r;
			this->hidden->vncs->colourMap.data.bytes[i++] = colours[x].g;
			this->hidden->vncs->colourMap.data.bytes[i++] = colours[x].b;
		}
		
		rfbSetClientColourMaps(this->hidden->vncs, firstcolour, ncolours);
		
		return 1;
	}
}


/* Note:  If we are terminated, this could be called in the middle of
   another SDL video routine -- notably UpdateRects.
*/
void VNC_VideoQuit(_THIS)
{
	rfbScreenInfoPtr vncscr = this->hidden->vncs;
	SDL_Surface* sdlscr = this->screen;

	rfbShutdownServer(vncscr, TRUE);

	/* Free SDL frame-buffer */
	if (sdlscr->pixels != NULL)
		SDL_free(sdlscr->pixels);

	/* Free VNC frame-buffer, but only when unshared */
	if (vncscr->frameBuffer != NULL) {
		if (vncscr->frameBuffer != sdlscr->pixels)
			SDL_free(vncscr->frameBuffer);

		vncscr->frameBuffer = NULL;
	}

	sdlscr->pixels = NULL;
}
