#ifndef __Y11_HELPERS_H
#define __Y11_HELPERS_H

#include <stdio.h>
#include <syslog.h>

/* Logging Helpers */
#define Y11_LOGF(_frmt_, ...) \
	{ printf("[Y11-VIDEO] "_frmt_"\n", ##__VA_ARGS__); \
	  syslog(LOG_USER | LOG_INFO, "[Y11-VIDEO] "_frmt_, ##__VA_ARGS__); }

#ifdef Y11_ENABLE_DEBUG_MESSAGES
#	define Y11_DEBUGF(_frmt_, ...)  Y11_LOGF(_frmt_, __VA_ARGS__)
#else
#	define Y11_DEBUGF(_frmt_, ...)  { }
#endif /* Y11_ENABLE_DEBUG_MESSAGES */

#endif /* __Y11_HELPERS_H */
